import os
from abc import ABC, abstractmethod

from osgeo import ogr

from .parser_cli import Parser

class ChangeAttributes(ABC):
    lower_values = True
    def __init__(self):
        # get arguments
        self.parser = Parser(lower_values=self.lower_values)

        print("SET search_path TO {},public;".format(os.environ["POSTGRES_SCHEMA"]))

    @staticmethod
    @abstractmethod
    def build_sql():
        pass

    def run(self):
        # read columns from input CSV file
        columns = self.parser.columns

        # perform operation on specified attributes
        self._run(columns,
                  self.parser.years
        )

    def _run(self, columns, years):
        # open connection to database
        ds = ogr.Open("PG:dbname={} user=postgres".format(os.environ["POSTGRES_DB"]))

        for year in years:
            print("DO $$ BEGIN RAISE NOTICE 'Processing {}'; END; $$;".format(year))
            print("BEGIN;")
            table = '{}.lucas{}'.format(os.environ["POSTGRES_SCHEMA"], year)
            lyr = ds.GetLayer(table)
            lyr_defn = lyr.GetLayerDefn()
            for i in range(lyr_defn.GetFieldCount()):
                field_name = lyr_defn.GetFieldDefn(i).GetName()
                if field_name in columns.keys():
                    self.build_sql(year, field_name, columns[field_name][self.item])
            print("COMMIT;")

        # close connection
        ds = None
