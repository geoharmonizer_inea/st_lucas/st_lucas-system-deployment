import os
import json
import argparse
from collections import OrderedDict

from db import Db, db_version

def publish_metadata(filename):
    mdata = {
        'version': db_version,
        'max_features': int(os.environ["GS_MAX_FEATURES"]),
        'stats': {
            'num_points': {},
            'years': {}
        }
    }

    db = Db()

    schema = os.environ["MAPSERVR_SCHEMA"]
    table = os.environ["TABLE_NAME"]
    table_st = os.environ["TABLE_NAME_ST"]

    # number of points
    mdata['stats']['num_points']['all'] = db.fetchone(
        f"SELECT count(*) FROM {schema}.{table}"
    )
    mdata['stats']['num_points']['st'] = db.fetchone(
        f"SELECT count(*) FROM {schema}.{table_st}"
    )

    # years
    for row in db.fetchall(
        f"select distinct survey_year from {schema}.{table} order by survey_year"):
        year = row[0]

        num_points = db.fetchone(
            f"SELECT count(*) FROM {schema}.{table} WHERE survey_year = {year}"
        )

        countries = db.fetchall(
            f"SELECT distinct nuts0 FROM {schema}.{table} WHERE survey_year = {year} ORDER BY nuts0"
        )
        countries_points = OrderedDict()
        for c in [item for t in countries for item in t]:
            # see https://gitlab.com/ctu-geoforall-lab/ctu-geoharmonizer/-/issues/176
            if c == '-1':
                continue
            countries_points[c] = db.fetchone(
                f"SELECT count(*) FROM {schema}.{table} WHERE survey_year = {year} and nuts0 = '{c}'"
            )

        mdata['stats']['years'][year] = {
            'countries': countries_points,
            'num_points': num_points
        }

    # append thematic attributes
    with open(os.path.join('conf', 'thematic_attributes.json'), 'r') as fd:
        mdata.update(json.load(fd))

    # write metadata
    with open(filename, 'w') as fd:
        json.dump(mdata, fd)

    # print out metadata
    # print(json.dumps(mdata, indent=4, sort_keys=True))
    print(f"Metadata published: {filename}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('output', metavar='json_file', type=str,
                        help='Path to output JSON file')
    args = parser.parse_args()

    publish_metadata(args.output)
