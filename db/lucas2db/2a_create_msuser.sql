--
-- create mapserver user
--
CREATE USER ${MAPSERVR_USER} with encrypted password '${MAPSERVR_PASSWORD}';
