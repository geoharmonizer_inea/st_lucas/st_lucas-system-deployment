--
-- 2009
--
UPDATE lucas2009 SET grazing = '-1' WHERE grazing = '3';
UPDATE lucas2009 SET soil_taken = '4' WHERE soil_taken = '3';
UPDATE lucas2009 SET th_ew = '1' WHERE th_ew = 'E';
UPDATE lucas2009 SET th_ew = '2' WHERE th_ew = 'W';
UPDATE lucas2009 SET crop_residues = '22' WHERE crop_residues = '1';
UPDATE lucas2009 SET crop_residues = '1' WHERE crop_residues in ('2','3','4');
UPDATE lucas2009 SET crop_residues = '2' WHERE crop_residues = '22';
UPDATE lucas2009 SET lc1_spec = '-1' WHERE lc1_spec = 'B77J';
UPDATE lucas2009 SET lc1_h = 'B23' WHERE lc1_spec = 'B43D';
UPDATE lucas2009 SET lc1_spec = 'B23N' WHERE lc1_spec = 'B43D';
UPDATE lucas2009 SET lc2_h = 'B23' WHERE lc2_spec = 'B43D';
UPDATE lucas2009 SET lc2_spec = 'B23N' WHERE lc2_spec = 'B43D';
UPDATE lucas2009 SET lc1_spec = 'B53B' WHERE lc1_spec = 'B53D';
UPDATE lucas2009 SET lc2_spec = 'B53B' WHERE lc2_spec = 'B53D';
UPDATE lucas2009 SET lc1_spec = 'C101' WHERE lc1_spec = 'C11';
UPDATE lucas2009 SET lc1_spec = 'C102' WHERE lc1_spec = 'C12';
UPDATE lucas2009 SET lc1_spec = 'C103' WHERE lc1_spec = 'C13';
UPDATE lucas2009 SET lc1_spec = 'C104' WHERE lc1_spec = 'C14';
UPDATE lucas2009 SET lc1_spec = 'C105' WHERE lc1_spec = 'C15';
UPDATE lucas2009 SET lc1_spec = 'C106' WHERE lc1_spec = 'C16';
UPDATE lucas2009 SET lc1_spec = 'C107' WHERE lc1_spec = 'C17';
UPDATE lucas2009 SET lc1_spec = 'C108' WHERE lc1_spec = 'C18';
UPDATE lucas2009 SET lc1_spec = 'C109' WHERE lc1_spec = 'C19';
UPDATE lucas2009 SET lc1_spec = 'C10A' WHERE lc1_spec = 'C1A';
UPDATE lucas2009 SET lc1_spec = 'C10B' WHERE lc1_spec = 'C1B';
UPDATE lucas2009 SET lc1_spec = 'C10C' WHERE lc1_spec = 'C1C';
UPDATE lucas2009 SET lc1_spec = 'C10D' WHERE lc1_spec = 'C1D';
UPDATE lucas2009 SET lc1_spec = 'C10E' WHERE lc1_spec = 'C1E';
UPDATE lucas2009 SET lc2_spec = 'C101' WHERE lc2_spec = 'C11';
UPDATE lucas2009 SET lc2_spec = 'C102' WHERE lc2_spec = 'C12';
UPDATE lucas2009 SET lc2_spec = 'C103' WHERE lc2_spec = 'C13';
UPDATE lucas2009 SET lc2_spec = 'C104' WHERE lc2_spec = 'C14';
UPDATE lucas2009 SET lc2_spec = 'C105' WHERE lc2_spec = 'C15';
UPDATE lucas2009 SET lc2_spec = 'C106' WHERE lc2_spec = 'C16';
UPDATE lucas2009 SET lc2_spec = 'C107' WHERE lc2_spec = 'C17';
UPDATE lucas2009 SET lc2_spec = 'C108' WHERE lc2_spec = 'C18';
UPDATE lucas2009 SET lc2_spec = 'C109' WHERE lc2_spec = 'C19';
UPDATE lucas2009 SET lc2_spec = 'C10A' WHERE lc2_spec = 'C1A';
UPDATE lucas2009 SET lc2_spec = 'C10B' WHERE lc2_spec = 'C1B';
UPDATE lucas2009 SET lc2_spec = 'C10C' WHERE lc2_spec = 'C1C';
UPDATE lucas2009 SET lc2_spec = 'C10D' WHERE lc2_spec = 'C1D';
UPDATE lucas2009 SET lc2_spec = 'C10E' WHERE lc2_spec = 'C1E';
-- Following lc1_spec and lc2_spec codes could not be harmonized to 2018 nomenclature due to the subdivision
-- of lc1 and lc2 classes C20 and C30 into C21, C22, C23 and C31, C32, C33 subclasses. Therefore we are not
-- able to say which specific lc1/2_spec codes it is.
UPDATE lucas2009 SET lc1_spec = '-1' WHERE lc1_spec in ('C21', 'C21E', 'C22', 'C23', 'C24', 'C25', 'C26', 'C27', 'C28', 'C29', 'C2A','C2B','C2C', 'C2D', 'C2E','C31', 'C32', 'C321', 'C325', 'C32A', 'C33', 'C34', 'C35', 'C36', 'C37', 'C38', 'C39', 'C3A', 'C3B', 'C3C', 'C3D', 'C3E');
UPDATE lucas2009 SET lc2_spec = '-1' WHERE lc2_spec in ('C21', 'C21E', 'C22', 'C23', 'C24', 'C25', 'C26', 'C27', 'C28', 'C29', 'C2A','C2B','C2C', 'C2D', 'C2E','C31', 'C32', 'C321', 'C325', 'C32A', 'C33', 'C34', 'C35', 'C36', 'C37', 'C38', 'C39', 'C3A', 'C3B', 'C3C', 'C3D', 'C3E');
UPDATE lucas2009 SET wm_delivery = '-1' where wm_delivery in ('0', '5','6','10','12');
UPDATE lucas2009 SET wm_source = '-1' where wm_source IN ('6','16','17','18','20','24');
UPDATE lucas2009 SET wm_type = '-1' where wm_type in ('0', '6','9','10','12','16','17','18','24');
UPDATE lucas2009 SET obs_type = '7' WHERE obs_type = '5';

--
-- LC harmonization
--
-- LC1
UPDATE lucas2009 SET lc1_h = 'Bx1' WHERE lc1 = 'BX1';
UPDATE lucas2009 SET lc1_h = 'Bx2' WHERE lc1 = 'BX2';
UPDATE lucas2009 SET lc1_h = '-1' WHERE lc1 IN ('C20', 'C30', 'F00', 'G10', 'G20');
-- LC2
UPDATE lucas2009 SET lc2_h = 'Bx1' WHERE lc2 = 'BX1';
UPDATE lucas2009 SET lc2_h = 'Bx2' WHERE lc2 = 'BX2';
UPDATE lucas2009 SET lc2_h = '-1' WHERE lc2 IN ('C20', 'C30', 'F00', 'G10', 'G20');

--
-- LU harmonization
--
-- LU1
UPDATE lucas2009 SET lu1_h = '-1' WHERE lu1 IN ('U340', 'U400', 'U364');
UPDATE lucas2009 SET lu1_h = 'U341' WHERE lu1 = 'U363';
-- LU2
UPDATE lucas2009 SET lu2_h = '-1' WHERE lu2 IN ('U340', 'U400', 'U364');
UPDATE lucas2009 SET lu2_h = 'U341' WHERE lu2 = 'U363';

--
-- Missing translation to level 3
--
-- LC1
UPDATE lucas2009 SET lc1_h_l3_missing = 'C20', lc1_h_l3_missing_level = 2 WHERE lc1 = 'C20';
UPDATE lucas2009 SET lc1_h_l3_missing = 'C30', lc1_h_l3_missing_level = 2 WHERE lc1 = 'C30';
UPDATE lucas2009 SET lc1_h_l3_missing = 'F00', lc1_h_l3_missing_level = 1 WHERE lc1 = 'F00';
UPDATE lucas2009 SET lc1_h_l3_missing = 'G10', lc1_h_l3_missing_level = 2 WHERE lc1 = 'G10';
UPDATE lucas2009 SET lc1_h_l3_missing = 'G20', lc1_h_l3_missing_level = 2 WHERE lc1 = 'G20';
-- LC2
UPDATE lucas2009 SET lc2_h_l3_missing = 'F00', lc2_h_l3_missing_level = 1 WHERE lc2 = 'F00';
