#!/bin/bash -e

# exit when tests are failing
set -o pipefail

export DIR=/opt/lucas2db
export DATA_DIR=/data
export CONF_DIR=$DIR/conf
export CODE_DIR=/coding

DUMP_FILE=${DATA_DIR}/dump.sql.7z

function run_psql {
    if test -z $1 ; then
        # stdin
        PGOPTIONS=--search_path=${POSTGRES_SCHEMA},public psql \
                 -d $POSTGRES_DB
    else
        envsubst \
            '$POSTGRES_SCHEMA $MAPSERVR_SCHEMA $MAPSERVR_USER $MAPSERVR_PASSWORD $YEARS $YEAR' \
            < $1 | \
            PGOPTIONS=--search_path=${POSTGRES_SCHEMA},public psql \
                     -d $POSTGRES_DB
    fi
}

function section_begin {
    echo "----------------------------------------------------------------"
    echo $1
    echo "----------------------------------------------------------------"
    SECONDS=0
}

function subsection {
    echo "----------------------------------------------------------------"
    echo $1
    echo "----------------------------------------------------------------"
}

function section_end {
    echo "VACUUM;" | run_psql
    SECONDS_DEPLOY=$(($SECONDS_DEPLOY + $SECONDS))
    time_elapsed $SECONDS
}

function time_elapsed {
    echo "TIME ELAPSED: $(( ${1} / 3600 ))h $(( (${1} / 60) % 60 ))m $(( ${1} % 60 ))s"
}

function run_test {
    if test -z $1 ; then
        python3 -m pytest -x -v -s -o cache_dir=/tmp/pytest_cache_dir $DIR/tests/test_1.py | tee -a /var/log/$LOGFILE
        python3 -m pytest -x -v -s -o cache_dir=/tmp/pytest_cache_dir $DIR/tests/test_2.py | tee -a /var/log/$LOGFILE
    else
        if [[ $1 = 1* ]] ; then
            t=1
        else
            t=2
        fi
        python3 -m pytest -x -v -s -o cache_dir=/tmp/pytest_cache_dir $DIR/tests/test_$t.py -k test_$1 | tee -a /var/log/$LOGFILE
    fi
}

function system_deployed {
    echo "----------------------------------------------------------------"
    echo "LUCAS SYSTEM SUCCESSFULLY DEPLOYED"
    echo "----------------------------------------------------------------"
    time_elapsed $SECONDS_DEPLOY
}

SECONDS_DEPLOY=0

# 1.
section_begin "(1) Download primary data"
python3 $DIR/1_download_primary_data.py ${CONF_DIR}/primary_data.json ${DATA_DIR}/primary
#  get YEARS variables
export `python3 $DIR/1_scan_primary_data.py $DATA_DIR/primary/points`
IFS=',' read -ra YEARS_ARRAY <<< "$YEARS"
# run_test 1
section_end

# 2.
section_begin "(2) Deploy database"
subsection "(2a) Initialize DB"
run_psql $DIR/2a_cleanup_db.sql
run_psql $DIR/2a_create_msuser.sql
# import from dump (if exists) and exit
if test -f ${DUMP_FILE} ; then
    echo "RESTORING FROM ${DUMP_FILE}..."
    7z e -so ${DUMP_FILE} | run_psql
    section_end
    run_test 2
    system_deployed
    exit 0
else
    run_psql $DIR/2a_grant_privileges.sql
    echo "ALTER SYSTEM SET autovacuum = off; SELECT PG_RELOAD_CONF();" | run_psql
fi
run_test 2a

subsection "(2b) Import primary data"
python3 $DIR/2b_import_primary_data.py $DATA_DIR/primary
run_test 2b
section_end

subsection "(2c) Harmonize coordinates"
run_psql $DIR/2c_harmonize_gps_values.sql
# import obs_radius_2018 which is used by 2c_create_geometries.py
ogr2ogr -f PGDump -lco SCHEMA=${POSTGRES_SCHEMA} -lco CREATE_SCHEMA=OFF -lco DROP_TABLE=OFF \
        /vsistdout/ conf/obs_radius_2018.csv | \
        run_psql
python3 $DIR/2c_create_geometries.py $YEARS | \
        run_psql
echo "Computing dist_thr_grid..."
for YEAR in "${YEARS_ARRAY[@]}"; do
    echo "Processing ${YEAR}..."
    export YEAR=$YEAR # required by envsubst
    run_psql $DIR/2c_dist_thr_grid.sql
done
run_test 2c

subsection "(2d) Harmonize attributes"
python3 $DIR/2d_harmonize_attributes.py ${CONF_DIR}/harmonize_attributes.csv \
        $YEARS | \
        run_psql
run_test 2d

subsection "(2e) Harmonize data values"
for YEAR in "${YEARS_ARRAY[@]}"; do
    echo "Processing ${YEAR}..."
    export YEAR=$YEAR # required by envsubst
    run_psql $DIR/2e_add_columns.sql
    if [ $YEAR -ne 2006 ] ; then
        run_psql $DIR/2e_harmonize_lc_spec.sql
    fi
    if test -f $DIR/2e_harmonize_values_${YEAR}.sql ; then
       run_psql $DIR/2e_harmonize_values_${YEAR}.sql
    fi
    run_psql $DIR/2e_harmonize_lc_lu.sql
    if [ $YEAR -gt 2006 -a $YEAR -lt 2018 ] ; then
        run_psql $DIR/2e_harmonize_perc_cls.sql
    fi
done

echo "Processing invalid values..."
python3 $DIR/2e_harmonize_invalid_values.py ${CONF_DIR}/harmonize_invalid_values.csv \
        $YEARS | \
        run_psql
run_test 2e

subsection "(2f) Harmonize data types"
python3 $DIR/2f_harmonize_data_types.py ${CONF_DIR}/harmonize_data_types.csv \
        $YEARS | \
        run_psql
run_test 2f

subsection "(2g) Merge harmonized data"
python3 $DIR/2g_merge_harmonized_data.py ${CONF_DIR}/list_of_attributes.csv \
        $YEARS $TABLE_NAME $TABLE_PRIMARY_KEY | \
        run_psql
run_test 2g

subsection "(2h) Space-time aggregation"
python3 $DIR/2h_aggregate_st.py ${CONF_DIR}/list_of_attributes.csv \
        $YEARS $TABLE_NAME_ST | \
        run_psql
run_test 2h

subsection "(2i) Create publication views"
python3 $DIR/2i_create_views.py ${CONF_DIR}/list_of_attributes.csv \
	${CONF_DIR}/thematic_attributes.json \
        $TABLE_NAME $TABLE_PRIMARY_KEY | \
        run_psql
python3 $DIR/2i_create_views_st.py ${CONF_DIR}/list_of_attributes.csv \
	$YEARS \
        ${CONF_DIR}/thematic_attributes.json \
        $TABLE_NAME_ST | \
        run_psql
run_test 2i

subsection "(2j) DB deployment finished"
echo "ALTER SYSTEM SET autovacuum = on; SELECT PG_RELOAD_CONF();" | run_psql
rm -f ${DUMP_FILE}
pg_dump -U postgres -d $POSTGRES_DB | 7z a -si ${DUMP_FILE}
run_test 2j
section_end

system_deployed

exit 0
