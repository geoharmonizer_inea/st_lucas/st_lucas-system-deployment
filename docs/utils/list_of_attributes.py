#!/usr/bin/python3

import os
import sys
import csv
import argparse
from pathlib import Path

sys.path.insert(0, '/opt/lucas2db')
from lib.parser_cli import read_groups

def list_of_codings(path):
    codings = []
    for p in Path(path).glob('*.csv'):
        codings.append(Path(p).stem.upper())
    return codings

def csv_publish(input_file, output_dir, json_file, codings_dir):
    with open(input_file, "r") as source:
        groups = read_groups(json_file)
        reader = csv.reader(source, delimiter=';')
        output_file = Path(output_dir) / Path(input_file).name
        codings = list_of_codings(codings_dir)
        with open(output_file, "w", newline='') as result:
            writer = csv.writer(result, delimiter=';')
            for r in reader:
                if r[2] == '1':
                    r[2] = 'Yes'
                elif r[2] == '0':
                    r[2] = 'No'
                # r[2] = 'Yes' if r[2] == '1' else 'No'
                if r[6] == '0':
                    r[6] = 'Primary'
                elif r[6] == '1':
                    r[6] = 'Harmonized'
                elif r[6] == '2':
                    r[6] = 'New'
                group_list = []
                for group in groups:
                    for item in groups[group]:
                        if r[1].lower() == item:
                            group_list.append(group)
                if len(group_list) > 0:
                    r[1] += ' ('+ ', '.join(map(lambda x: x.upper(), group_list)) +')'
                row_output = [r[0], r[1], r[4], r[5], r[6]] # skip Base and Domain
                if r[0] in codings:
                    row_output[-2] = '`Coding table <./coding/{}.html>`_'.format(r[0].lower())
                elif r[5]:
                    row_output[-2] = ''
                    for r in r[5].split(','):
                        row_output[-2] += '| {}\n'.format(r)
                writer.writerow(row_output)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', metavar='input_file', type=str,
                        help='Input CSV with list of attributes')
    parser.add_argument('json_file', metavar='json_file', type =str,
                        help='Input JSON file with groups')
    parser.add_argument('codings_dir', metavar='codings_dir', type =str,
                        help='Input directory with coding tables')
    parser.add_argument('output_dir', metavar='output_dir', type=str,
                        help='Output directory')
    args = parser.parse_args()

    csv_publish(args.input_file, args.output_dir, args.json_file, args.codings_dir)
