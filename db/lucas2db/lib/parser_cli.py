import csv
import argparse
import json
from collections import OrderedDict

def read_groups(filename):
    with open(filename) as fd:
        data = json.load(fd)

    groups = {}
    for item in data["thematic_attributes"].values():
        for group, topics in item.items():
            group = group.lower()
            if group not in groups.keys():
                groups[group] = set()
            groups[group].update(
                list(map(lambda x: x.lower(), topics))
            )

    return groups

def read_csv(csv_file, lower_values=True):
    """Read CSV file.

    :return list, list: two column returned as lists
    """
    columns = OrderedDict()
    with open(csv_file, newline='') as fd:
        reader = csv.DictReader(fd, delimiter=';')
        for row in reader:
            # https://stackoverflow.com/questions/1885324/is-it-possible-to-keep-the-column-order-using-csv-dictreader
            try:
                sorted_row = OrderedDict(
                    sorted(row.items(),
                           key=lambda item: reader.fieldnames.index(item[0]))
                )
            except ValueError:
                raise Exception(f"Invalid row: {row}")

            key = list(sorted_row.keys())
            items = {}
            for k in key[1:]:
                items[k.lower()] = sorted_row[k].lower() if lower_values else sorted_row[k]
            columns[sorted_row[key[0]].lower()] = items

    return columns

class Parser:
    def __init__(self, csv_file=True, years=True, lower_values=True, args=[]):
        parser = argparse.ArgumentParser()
        self._lower_values = lower_values
        self._known_attrs = []
        if csv_file:
            parser.add_argument('csv_file', metavar='csv_file', type=str,
                                help='Path to input CSV file')
            self._known_attrs.append("csv_file")
        if years:
            parser.add_argument('years', metavar='years', type=str,
                                help='List of years to be processed')
            self._known_attrs.extend(["years"])

        for arg in args:
            parser.add_argument(**arg)
            self._known_attrs.append(arg['dest'])
    
        self._args = parser.parse_args()
        
    def __getattr__(self, attr):
        if attr in self._known_attrs:
            if attr == "years":
                return list(map(int, self._args.__getattribute__(attr).split(',')))
            else:
                return self._args.__getattribute__(attr)
        elif attr == "columns":
            return read_csv(self._args.csv_file, self._lower_values)
        else:
            raise Exception("Invalid attribute {}".format(attr))
        
