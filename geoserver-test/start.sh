#!/bin/bash -e

# exit when tests are failing
set -o pipefail

# wait for Geoserver
while true; do
    python3 -m pytest -x -v -s -o cache_dir=/tmp/pytest_cache_dir ./tests/test_3.py -k 3a_001 >/dev/null
    if [ $? -eq 0 ] ; then
        echo "GeoServer is ready. Running tests..."
        python3 -m pytest -x -v -s -o cache_dir=/tmp/pytest_cache_dir ./tests/test_3.py -k 3a | tee -a /var/log/$LOGFILE
        break
    fi
    echo "Waiting for GeoServer..."
    sleep 60
done

while true; do
    python3 -m pytest -x -v -s -o cache_dir=/tmp/pytest_cache_dir ./tests/test_3.py -k 3b_001 >/dev/null
    if [ $? -eq 0 ] ; then
        break
    fi
    echo "Waiting for data publication..."    
    sleep 60
done

echo "DEPLOYMENT FINISHED:" `date --rfc-3339=seconds` >> /var/log/$LOGFILE

while true; do
    # update python package lib
    cd st_lucas-python-package
    git pull
    python3 -m pip install --upgrade .
    cd ..
    # run tests
    echo "Performing integration tests..."
    LOGFILE=/var/log/integration_tests_st_lucas-`date -Is`.log
    RETCODE=0
    echo "============================= INTEGRATION TESTS ==============================" > $LOGFILE
    python3 -m pytest -v -s -o cache_dir=/tmp/pytest_cache_dir ./tests/test_if1_if2.py --url $HOSTNAME >> $LOGFILE
    python3 -m pytest -v -s -o cache_dir=/tmp/pytest_cache_dir ./st_lucas-python-package/tests/test_st_lucas.py --url $HOSTNAME >> $LOGFILE
    RETCODE=$((RETCODE+$?))
    python notifier.py $RETCODE $LOGFILE
    echo "Sleeping for ${GS_TEST_REPEAT}hrs..."
    sleep $(($GS_TEST_REPEAT * 60 * 60))
done

exit 0
