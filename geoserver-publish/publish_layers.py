import os
import psycopg2
import time
from geo.Geoserver import Geoserver, GeoserverException

from db import Db

def clear_all(layers_list):
    '''
    Deletes the datastore and its content.
    :param layers_list: list of layers to delete form the datastore
    :return:
    '''
    # delete layers
    for layer_name in layers_list:
        try:
            geo.delete_layer(layer_name=layer_name, workspace=workspace)
        except GeoserverException:
            pass # assuming 404

    # delete feature store, i.e. remove postgresql connection
    try:
        geo.delete_featurestore(featurestore_name=featurestore, workspace=workspace)
    except GeoserverException:
        pass # assuming 404

    # delete workspace
    try:
        geo.delete_workspace(workspace=workspace)
    except GeoserverException:
        pass # assuming 404


def publish_layers(layers_list):
    '''
    Publishes layers form the postgresql database.
    :param layers_list: list of layers to publish
    :return:
    '''
    # For creating workspace
    geo.create_workspace(workspace=workspace)

    # For creating postGIS connection and publish postGIS table
    geo.create_featurestore(store_name=featurestore, workspace=workspace, db=db, host=host, pg_user=pg_user,
                            pg_password=pg_password, schema=schema)

    for layer_name in layers_list:
        geo.publish_featurestore(workspace=workspace, store_name=featurestore, pg_table=layer_name)


def get_layers_list():
    '''
    Returns all tables and views in the schema.
    :return: all tables and views in the schema
    '''
    try:
        records = Db().fetchall(
            f"SELECT table_name FROM information_schema.tables WHERE table_schema='{schema}'"
        )
    except psycopg2.Error as e:
        print(e)
        return []

    layers = []
    with open('layers.txt', 'w') as out:
        for record in records:
            layers.append(record[0])
            print(record[0])
            out.write(record[0] + '\n')

    return layers

if __name__ == "__main__":
    delete_first = True

    geoserver_url = 'http://gs:8080/geoserver'
    geoserver_user = 'admin'
    geoserver_password = os.environ['GS_ADMIN_PASSWORD']

    pg_user= os.environ['MAPSERVR_USER']
    pg_password = os.environ['MAPSERVR_PASSWORD']
    host = 'db'
    db = os.environ['POSTGRES_DB']
    schema = os.environ['MAPSERVR_SCHEMA']

    workspace = os.environ['POSTGRES_DB']
    featurestore= os.environ['POSTGRES_DB']

    # Initialize the library
    geo = Geoserver(geoserver_url, username=geoserver_user, password=geoserver_password)

    not_published = True
    while not_published:
        time.sleep(60)
        print("Checking layers to publish...")
        layers_list = get_layers_list()
        if len(layers_list) > 0:
            if delete_first:
                clear_all(layers_list)
            publish_layers(layers_list)
            not_published = False
        else:
            print("Nothing to publish. The database returned zero layers. Sleeping for 60s")
