#!/bin/bash -e

EXTRACTSDIR=$BUILDDIR/html/extracts/

function extract_layer {
    layer_name=$1
    ogr2ogr -f GPKG -overwrite $EXTRACTSDIR/${layer_name}.gpkg \
            "PG:dbname=$POSTGRES_DB host=db user=$MAPSERVR_USER password=$MAPSERVR_PASSWORD" \
            ${MAPSERVR_SCHEMA}.${layer_name}
    (cd $EXTRACTSDIR; 7z a ${layer_name}.gpkg.7z ${layer_name}.gpkg)
}

# create extracts
mkdir $EXTRACTSDIR
cp /data/dump.sql.7z $EXTRACTSDIR/db_st_lucas_dump.sql.7z
echo "DB dump file copied: $EXTRACTSDIR"

extract_layer ${TABLE_NAME}
extract_layer ${TABLE_NAME_ST}
echo "GPKG extract created in $EXTRACTSDIR"

exit 0
