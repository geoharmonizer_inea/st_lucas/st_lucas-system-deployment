# ST_LUCAS System Deployment 

ST_LUCAS system is implemented as a composition of services running in
isolated Docker containers. Deployment of the services is managed by a
`docker compose` tool.

<img src="docs/_static/st_lucas_architecture.png" width="650"/><br>
<i>ST_LUCAS system architecture.</i>

## Requirements

Install on OS host level [Docker](https://docker.com) software.

## Build Docker images

Docker images are built by:

```sh
docker compose build
```

## Configure the system

Configuration is defined by a set of environmental variables.

Create `env` file in the root directory and specify required
environmental variables below:

* `POSTGRES_PASSWORD` (required) - database administrator password
* `POSTGRES_DB` (required) - database name
* `POSTGRES_SCHEMA` (required) - database schema for data harmonization
* `MAPSERVR_SCHEMA` (required) - database schema for publication
* `MAPSERVR_USER` (required) - database username used by a map server
* `MAPSERVR_PASSWORD` (required) - database map server password
* `GS_ADMIN_PASSWORD` (required) - map server adminitrator password
* `GS_MAX_FEATURES` (required) - map server max features settings
* `GS_TEST_REPEAT` (required) - repeat integration tests every X hours
* `TABLE_NAME` (required) - database table name for harmonized LUCAS data
* `TABLE_NAME_ST` (required) - database table name for harmonized space-time aggregated LUCAS data
* `TABLE_PRIMARY_KEY` (required) - database column name for feature identifier
* `LOGFILE` (required) - unit tests log filename
* `HOSTNAME` (required) - hostname
* `EMAIL_USERNAME` (optional) - SMTP username for sending administrator notifications
* `EMAIL_PASSWORD` (optional) - SMTP password for sending administrator notifications
* `EMAIL_FROMADDR` (optional) - administrator (from) e-mail address
* `EMAIL_MAILHOST` (optional) - SMTP server name for sending administrator notifications
* `EMAIL_MAILPORT` (optional) - SMTP server port for sending administrator notifications
* `EMAIL_TOADDRS` (optional) - list of e-mail addresses (seperated by comma) where to send administrator notifications

Example of the `env` file:

```sh
POSTGRES_PASSWORD=very_secret_pw0
POSTGRES_DB=lucas
POSTGRES_SCHEMA=data
MAPSERVR_SCHEMA=ms
MAPSERVR_USER=mapserver
MAPSERVR_PASSWORD=very_secret_pw0
GS_ADMIN_PASSWORD=very_secret_pw0
GS_MAX_FEATURES=300000
GS_TEST_REPEAT=24
TABLE_NAME=lucas_points
TABLE_NAME_ST=lucas_st_points
TABLE_PRIMARY_KEY=fid
LOGFILE=unit_tests_st_lucas.log
HOSTNAME=http://localhost:8081
```

## Deploy the system

Set up permissions:

```sh
chmod a+w db/data_dir/
```

Run:

```sh
docker compose up
```

### Repeated deployment

LUCAS data harmonization process is performed only on the first
deployment. In the case of repeated deployment the database is
recovered from a DB dump file. To force harmonization process the DB
dump file, primary data and the `dcdata` and `gsdata` volumes has to
be deleted.

Stop services if running:

```sh
docker compose down
```

Delete DB dump file, primary data and the volumes:

```sh
rm -r db/data_dir/dump.sql.7z db/data_dir/primary/
docker volume rm st_lucas-system-deployment_dcdata st_lucas-system-deployment_gsdata
```

Deploy the system:

```sh
docker compose up
```

## Notes

### Access deployed database

Access DB as administrator (use password defined by
`POSTGRES_PASSWORD` environmental variable):

```sh
docker compose exec db psql -U postgres -d lucas
```

### Run integration tests manually

```sh
docker compose exec db bash
YEARS=2006,2009,2012,2015,2018 DATA_DIR=/data CONF_DIR=./conf CODE_DIR=/coding python3 -m pytest -xvs tests/test_2.py
```

```
docker compose exec gst bash
python3 -m pytest -xvs tests/test_if1_if2.py
```

### Update documentation

```sh
docker compose run --rm gsp
```

## Funding

This work is co-financed under Grant Agreement Connecting Europe
Facility (CEF) Telecom project 2018-EU-IA-0095 by the European Union.
