import os
import psycopg2

db_version = 1.1

class Db:
    user= os.environ['MAPSERVR_USER']
    password = os.environ['MAPSERVR_PASSWORD']
    db = os.environ['POSTGRES_DB']
    host = 'db'
    port = 5432

    schema = os.environ['MAPSERVR_SCHEMA']
    workspace = os.environ['POSTGRES_DB']
    featurestore= os.environ['POSTGRES_DB']

    def __init__(self):
        conn_string = f"dbname='{self.db}' port='{self.port}' user='{self.user}' " \
            f"password='{self.password}' host='{self.host}'"

        self.conn = psycopg2.connect(conn_string)

    def __del__(self):
        if hasattr(self, 'conn'):
            self.conn.close()

    def fetchall(self, sql):
        with self.conn.cursor() as cur:
            cur.execute(sql)
            res = cur.fetchall()
        return res

    def fetchone(self, sql):
        with self.conn.cursor() as cur:
            cur.execute(sql)
            res = cur.fetchone()
        return res[0]
