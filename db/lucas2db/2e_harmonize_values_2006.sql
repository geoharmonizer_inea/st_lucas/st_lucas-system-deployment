--
-- 2006
--
UPDATE lucas2006 SET obs_type = '7' WHERE obs_type = '2';
UPDATE lucas2006 SET obs_type = '2' WHERE obs_type = '1' AND obs_dist::float > 100;
UPDATE lucas2006 set survey_date = '30/06/2006' WHERE survey_date = '88/88/2006';
 
--
-- LC harmonization
--
-- LC1
UPDATE lucas2006 SET lc1_h = 'C10' WHERE lc1 = 'C11';
UPDATE lucas2006 SET lc1_h = '-1' WHERE lc1 = 'C12';
UPDATE lucas2006 SET lc1_h = '-1' WHERE lc1 = 'C13';
UPDATE lucas2006 SET lc1_h = 'C10' WHERE lc1 = 'C21';
UPDATE lucas2006 SET lc1_h = '-1' WHERE lc1 = 'C22';
UPDATE lucas2006 SET lc1_h = '-1' WHERE lc1 = 'C23';
UPDATE lucas2006 SET lc1_h = 'D10' WHERE lc1 = 'D01';
UPDATE lucas2006 SET lc1_h = 'D20' WHERE lc1 = 'D02';
UPDATE lucas2006 SET lc1_h = 'E10' WHERE lc1 = 'E01';
UPDATE lucas2006 SET lc1_h = 'E20' WHERE lc1 = 'E02';
UPDATE lucas2006 SET lc1_h = '-1' WHERE lc1 = 'F00';
UPDATE lucas2006 SET lc1_h = '-1' WHERE lc1 = 'G01';
UPDATE lucas2006 SET lc1_h = '-1' WHERE lc1 = 'G02';
UPDATE lucas2006 SET lc1_h = 'G30' WHERE lc1 = 'G03';
UPDATE lucas2006 SET lc1_h = 'G50' WHERE lc1 = 'G05';
-- LC2
UPDATE lucas2006 SET lc2_h = '-1' WHERE lc2 = '0';
UPDATE lucas2006 SET lc2_h = 'C10' WHERE lc2 = 'C11';
UPDATE lucas2006 SET lc2_h = '-1' WHERE lc2 = 'C12';
UPDATE lucas2006 SET lc2_h = '-1' WHERE lc2 = 'C13';
UPDATE lucas2006 SET lc2_h = 'C10' WHERE lc2 = 'C21';
UPDATE lucas2006 SET lc2_h = '-1' WHERE lc2 = 'C22';
UPDATE lucas2006 SET lc2_h = '-1' WHERE lc2 = 'C23';
UPDATE lucas2006 SET lc2_h = 'D10' WHERE lc2 = 'D01';
UPDATE lucas2006 SET lc2_h = 'D20' WHERE lc2 = 'D02';
UPDATE lucas2006 SET lc2_h = 'E10' WHERE lc2 = 'E01';
UPDATE lucas2006 SET lc2_h = 'E20' WHERE lc2 = 'E02';
UPDATE lucas2006 SET lc2_h = '-1' WHERE lc2 = 'F00';
UPDATE lucas2006 SET lc2_h = '-1' WHERE lc2 = 'G01';
UPDATE lucas2006 SET lc2_h = '-1' WHERE lc2 = 'G02';
UPDATE lucas2006 SET lc2_h = 'G30' WHERE lc2 = 'G03';
UPDATE lucas2006 SET lc2_h = 'G50' WHERE lc2 = 'G05';
UPDATE lucas2006 SET lc2_h = '-1' WHERE lc2 = '000';

--
-- LU harmonization
--
-- LU1
UPDATE lucas2006 SET lu1_h = '-1' WHERE lu1 = '0';
UPDATE lucas2006 SET lu1_h = '-1' WHERE lu1 = 'U340';
UPDATE lucas2006 SET lu1_h = 'U341' WHERE lu1 = 'U363';
UPDATE lucas2006 SET lu1_h = '-1' WHERE lu1 = 'U400';
UPDATE lucas2006 SET lu1_h = '-1' WHERE lu1 = 'U500';
UPDATE lucas2006 SET lu1_h = '-1' WHERE lu1 = '0000';
-- LU2
UPDATE lucas2006 SET lu2_h = '-1' WHERE lu2 = '0';
UPDATE lucas2006 SET lu2_h = '-1' WHERE lu2 = 'U340';
UPDATE lucas2006 SET lu2_h = 'U341' WHERE lu2 = 'U363';
UPDATE lucas2006 SET lu2_h = '-1' WHERE lu2 = 'U400';
UPDATE lucas2006 SET lu2_h = '-1' WHERE lu2 = 'U500';
UPDATE lucas2006 SET lu2_h = '-1' WHERE lu2 = '0000';

--
-- Missing translation to level 3
--
-- LC1
UPDATE lucas2006 SET lc1_h_l3_missing = 'C20', lc1_h_l3_missing_level = 2 WHERE lc1 in ('C12', 'C22');
UPDATE lucas2006 SET lc1_h_l3_missing = 'C30', lc1_h_l3_missing_level = 2 WHERE lc1 in ('C13', 'C23');
UPDATE lucas2006 SET lc1_h_l3_missing = 'F00', lc1_h_l3_missing_level = 1 WHERE lc1 = 'F00';
UPDATE lucas2006 SET lc1_h_l3_missing = 'G10', lc1_h_l3_missing_level = 2 WHERE lc1 = 'G01';
UPDATE lucas2006 SET lc1_h_l3_missing = 'G20', lc1_h_l3_missing_level = 2 WHERE lc1 = 'G02';
-- LC2
UPDATE lucas2006 SET lc2_h_l3_missing = 'C20', lc2_h_l3_missing_level = 2 WHERE lc2 in ('C12', 'C22');
UPDATE lucas2006 SET lc2_h_l3_missing = 'C30', lc2_h_l3_missing_level = 2 WHERE lc2 in ('C13', 'C23');
UPDATE lucas2006 SET lc2_h_l3_missing = 'F00', lc2_h_l3_missing_level = 1 WHERE lc2 = 'F00';
UPDATE lucas2006 SET lc2_h_l3_missing = 'G10', lc2_h_l3_missing_level = 2 WHERE lc2 = 'G01';
UPDATE lucas2006 SET lc2_h_l3_missing = 'G20', lc2_h_l3_missing_level = 2 WHERE lc2 = 'G02';

--
-- U500 Wetlands
--
UPDATE lucas2006 SET lc1_h_l3_missing = 'H00', lc1_h_l3_missing_level = 1, lc1_h = -1 WHERE lu1 = 'U500';
