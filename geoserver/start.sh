# clean-up
rm -rf /opt/sphinx/build/html
mkdir -p /opt/sphinx/build/html
rm -rf /opt/logs/*
echo "============================= UNIT TESTS =======================================" > /opt/logs/$LOGFILE
echo "DEPLOYMENT STARTED:" `date --rfc-3339=seconds` >> /opt/logs/$LOGFILE
chmod a+w /opt/logs/$LOGFILE

# start tomcat and geoserver
sed -i "s/\/display-name>/\/display-name><context-param><param-name>GEOSERVER_DATA_DIR<\/param-name><param-value>\/var\/local\/geoserver<\/param-value><\/context-param>/g" /usr/local/geoserver/WEB-INF/web.xml
export GEOSERVER_DATA_DIR=/var/local/geoserver/
bash /usr/local/bin/start.sh &
sleep 60

# If necessary may be rewritten with curl for Python
echo "Changing the password"
curl -u admin:geoserver -X PUT http://gs:8080/geoserver/rest/security/self/password -H  "accept: application/json" -H  "content-type: application/json" -d "{  \"newPassword\": \"$GS_ADMIN_PASSWORD\"}"
echo "Cleaning the cache"
curl -u admin:$GS_ADMIN_PASSWORD -X PUT http://gs:8080/geoserver/rest/reset -H  "accept: application/json"

# If necessary may be rewritten with curl for Python
echo "Changing the password"
curl -u admin:geoserver \
     -X PUT http://localhost:8080/geoserver/rest/security/self/password \
     -H  "accept: application/json" \
     -H  "content-type: application/json" \
     -d "{  \"newPassword\": \"$GS_ADMIN_PASSWORD\"}"
echo "Cleaning the cache"
curl -u admin:$GS_ADMIN_PASSWORD \
     -X PUT http://localhost:8080/geoserver/rest/reset \
     -H  "accept: application/json"

# keep container running
while true; do
	sleep 600
	echo "GeoServer is running ..."
done

exit 0
