#!/usr/bin/env python3

import sys
import os
import pytest
import psycopg2
import copy
from pathlib import Path

from owslib.fes import PropertyIsEqualTo, Or

from st_lucas import LucasRequest, LucasIO

from utils import Db

def _setup(request, url=None):
    request.cls.url = url

@pytest.fixture(scope='class')
def class_manager(request, pytestconfig):
    url = pytestconfig.getoption("url")
    _setup(request, url)
    yield

@pytest.mark.usefixtures('class_manager')
class TestIF1_IF2(Db):
    bbox = (4626445, 2980703, 4682551, 3009293)
    countries = ['MT', 'LU']
    years = [2006, 2009]
    propertyname = 'lc1_h'
    literal = 'A%'
    
    def _sql_vals(self, vals):
        if type(vals[0]) == int:
            return ','.join(map(lambda x: str(x), vals))
        else:
            return ','.join(map(lambda x: "'"+x+"'", vals))
        
    def __test(self, req, sql):
        args = {}
        if self.url:
            args['url'] = self.url
        lucasio = LucasIO(**args)
        lucasio.download(req)
        with self._connect() as conn:
            with conn.cursor() as cur:
                count = self._fetchone(
                    cur,
                    sql
            )
        assert lucasio.count() == count

    def _test(self, req, sql_where, skip_st=False):
        self.__test(
            req,
            f"SELECT count(*) FROM {self.ms_schema}.lucas_points WHERE {sql_where}"
        )
        if skip_st is False:
            req.st_aggregated = True
            self.__test(
                req,
                f"SELECT count(*) FROM {self.ms_schema}.lucas_st_points WHERE {sql_where}"
            )

    def _test_spatial(self, req_orig, sql='', skip_st=False):
        # bbox
        req = copy.deepcopy(req_orig)
        req.bbox = self.bbox
        self._test(
            req,
            f"geom && ST_MakeEnvelope({self._sql_vals(self.bbox)}, 3035)"+sql,
            skip_st
        )

        # countries
        req = copy.deepcopy(req_orig)
        req.countries = self.countries
        sql_countries = ','.join(map(lambda x: "'"+x+"'", req.countries))
        self._test(
            req,
            f"nuts0 in ({self._sql_vals(req.countries)})"+sql,
            skip_st
        )

        # aoi
        def build_sql(table):
            return f"SELECT count(points.*) FROM {self.ms_schema}.{table} as points " \
                   f"JOIN ST_Polygon('LINESTRING(4558681 3037623, 4688645 3147130, 4796949 3088165, 4776491 2982268, 4632087 2934133, 4558681 3037623)'::geometry, 3035) as polygon " \
                   f"ON ST_WITHIN(points.geom, polygon)"+sql

        req = copy.deepcopy(req_orig)
        req.aoi_polygon = '<Within>'  \
            '     <PropertyName>lucas:geom</PropertyName>' \
            '     <gml:Polygon xmlns:gml="http://www.opengis.net/gml" srsName="urn:ogc:def:crs:EPSG::3035" gml:id="polygon_32.geom.0">' \
            '         <gml:exterior>' \
            '             <gml:LinearRing>' \
            '                 <gml:posList>3037623 4558681 3147130 4688645 3088165 4796949 2982268 4776491 2934133 4632087 3037623 4558681 </gml:posList>' \
            '             </gml:LinearRing>' \
            '         </gml:exterior>' \
            '     </gml:Polygon>' \
            '</Within>'

        self.__test(
            req,
            build_sql('lucas_points'),
        )

        if not skip_st:
            req.st_aggregated = True
            self.__test(
                req,
                build_sql('lucas_st_points')
            )

    def test_001(self):
        """Spatial filters (bbox, countries, polygon).

        This tests case consists of checking that spatial filters work
        as expected (IF2). Response provided by web service is
        compared with recordset retrieved from the database (IF1).
        """
        req = LucasRequest()
        self._test_spatial(
            req
        )

    def test_002(self):
        """Combination of spatial (bbox, countries, polygon) and temporal (years) filter.

        This tests case consists of checking that combination of
        spatial and temporal filters work as expected (IF2). Response
        provided by web service is compared with recordset retrieved
        from the database (IF1).
        """
        req = LucasRequest()
        req.years = self.years

        self._test_spatial(
            req,
            f" and survey_year in ({self._sql_vals(req.years)})",
            skip_st=True
        )

    def test_003(self):
        """Combination of spatial (bbox, countries, polygon) and attribute filter.

        This tests case consists of checking that combination of
        spatial and attribute filters work as expected (IF2). Response
        provided by web service is compared with recordset retrieved
        from the database (IF1).
        """
        from owslib.fes import PropertyIsLike

        req = LucasRequest()
        req.operator=PropertyIsLike
        req.propertyname = self.propertyname
        req.literal = self.literal

        self._test_spatial(
            req,
            f" and {req.propertyname} like '{req.literal}'",
            skip_st=True
        )

    def test_004(self):
        """Combination of spatial (bbox), temporal and attribute filter.

        This tests case consists of checking that combination of
        spatial (bbox), temporal and attribute filter works as expected
        (IF2). Response provided by web service is compared with
        recordset retrieved from the database (IF1).
        """
        from owslib.fes import PropertyIsLike

        req = LucasRequest()
        req.years = self.years
        req.operator=PropertyIsLike
        req.propertyname = self.propertyname
        req.literal = self.literal

        self._test_spatial(
            req,
            f" and {req.propertyname} like '{req.literal}'"
            f" and survey_year in ({self._sql_vals(req.years)})",
            skip_st=True
        )
