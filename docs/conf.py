# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
# Problems with imports? Could try `export PYTHONPATH=$PYTHONPATH:`pwd`` from root project dir...
import os
import sys
sys.path.insert(0, os.path.abspath('../'))  # Source code dir relative to this file


# -- Project information -----------------------------------------------------

# The short X.Y version
version = '1.3dev'
# The full version, including alpha/beta/rc tags
release = version

project = 'ST_LUCAS'
copyright = '2019-2022, Geo-harmonizer project team; 2023-2024 CTU GeoForAll Lab'
author = 'CTU GeoForAll Lab'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.todo',
]

# Mappings for sphinx.ext.intersphinx. Projects have to have Sphinx-generated doc! (.inv file)
intersphinx_mapping = {
#    "python": ("https://docs.python.org/3/", None),
}

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']
html_context = { 'HOSTNAME': os.getenv('HOSTNAME', '') }

# Pydata theme
html_theme = "pydata_sphinx_theme"
html_logo = "_static/logo.png"
html_theme_options = {
    "collapse_navigation" : False,
    "icon_links": [
        {
            "name": "GitLab",
            "url": "https://gitlab.com/geoharmonizer_inea/st_lucas",
            "icon": "fab fa-gitlab",
        }
    ],
   "external_links": [                                                                                           
      {"name": "ST_LUCAS", "url": "https://geoforall.fsv.cvut.cz/st_lucas"},
      {"name": "ST_LUCAS API", "url": "https://geoforall.fsv.cvut.cz/st_lucas/api/"},
      {"name": "ST_LUCAS QGIS Plugin", "url": "https://geoforall.fsv.cvut.cz/st_lucas/qgis_plugin/"},
    ],           
    "show_prev_next": False,
    "navbar_end": ["navbar-icon-links.html"],
    "navbar_align": "left",
    "navbar_center": ["major_links"],
    "show_nav_level": 2,
    "footer_start": ["copyright"],
    "footer_center": [],
    "footer_end": ["version"],
}
html_sidebars = {
#    "**": ["sidebar-nav-bs.html", "sidebar-ethical-ads.html"]
    "**": []
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# Display todos by setting to True
todo_include_todos = True
