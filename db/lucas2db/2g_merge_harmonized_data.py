#!/usr/bin/env python3

import os

from lib.parser_cli import Parser
from lib.merge_points import MergePoints

class MergeYears(MergePoints):
    def __init__(self, table, columns, primary_key, years):
        self.primary_key = primary_key

        super().__init__(table, columns, years, exclude=["SPACE-TIME"])

    def build_sql(self):
        print("BEGIN;")
        print("CREATE TABLE {} ({} serial,{});".format(
            self.table, self.primary_key,
            ','.join(
                map(lambda x: '{} {}'.format(x, self.columns[x]["dtype"]), list(self.columns.keys()))),
        ))

        for year in self.years:
            columns_year = []
            for name in self.columns.keys():
                if year not in self.columns[name]["exclude"]:
                    columns_year.append(name)

            print("DO $$ BEGIN RAISE NOTICE 'Processing {}'; END; $$;".format(year))
            print("INSERT INTO {t} ({c1}) (SELECT {c2},{y} FROM lucas{y});".format(
                t=self.table, c1=','.join(columns_year),
                c2=','.join([x for x in columns_year if x not in self.additional]),
                y=year,
            ))

        self.create_indices(["point_id", "nuts0", "survey_date", "survey_year",
                             "geog_gps", "geom_gps", "geom_obs_radius", "geom_cprn_lc1nsew",
                             "geog_th", "geom_thr", "geom"]
        )

        print("COMMIT;")

if __name__ == "__main__":
    parser = Parser(
        args=[
            { 'dest': 'table', 'metavar': 'table', 'type': str,
              'help': 'Table name'
             },
            { 'dest': 'pkey', 'metavar': 'pkey', 'type': str,
              'help': 'Primary key'
             },
        ]
    )

    my = MergeYears(parser.table, parser.columns, parser.pkey,
                    parser.years
    )
    my.build_sql()
