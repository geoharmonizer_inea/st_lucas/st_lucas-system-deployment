import os
import sys
import json
import urllib.request
import py7zr
import argparse
from zipfile import ZipFile

def download(input_json, data_dir):
    with open(input_json) as json_file:
        data = json.load(json_file)
    for dirname in data.keys():
        target_dir = os.path.join(data_dir, dirname)
        if not os.path.exists(target_dir):
            os.makedirs(target_dir)
        for url in data[dirname]:
            filename = os.path.join(target_dir, os.path.basename(url))
            # fix XLS files from 2006, content is CSV(!)
            if os.path.splitext(filename)[1] == '.xls':
                filename = os.path.splitext(filename)[0] + '.csv'
            if not os.path.exists(filename):
                print("Downloading {}...".format(filename), file=sys.stderr)
                sys.stderr.flush()
                urllib.request.urlretrieve(url, filename)
            if os.path.splitext(filename)[1] == '.7z':
                with py7zr.SevenZipFile(filename, mode='r') as z:
                    z.extractall(path=os.path.dirname(filename))
            elif os.path.splitext(filename)[1] == '.zip':
                with ZipFile(filename) as fd:
                    fd.extractall(path=os.path.dirname(filename))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('input', metavar='json_file', type=str,
                        help='Path to input JSON file')
    parser.add_argument('output_dir', metavar='output_dir', type=str,
                        help='Path to output directory')

    args = parser.parse_args()

    download(args.input, args.output_dir)
