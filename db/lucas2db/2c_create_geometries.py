import os
import sys

from lib.parser_cli import Parser

# geog_th already built by 02b
# new geographies
#  * geog_gps: built from gps_lat and gps_long
# new geometries
#  * geom_gps: transformed from geog_gps to EPSG 3035
#  * geom_thr: geog_th snapped to the grid and transformed to EPSG 3035
#  * geom_obs_radius built from obs_radius
#  * geom_cprn_lc1nsew: built from cprnc_lc1n|s|e|w attributes
# distance attributes:
#  * obs_dist: distance between geom_gps and geom_thr

def create_geometries(years):
    for year in years:
        col_prefix = "survey_" if year > 2018 else ""

        # geog_gps (4326)
        if year > 2018:
            print("UPDATE lucas{0} SET {1}gps_long=-1,{1}gps_lat=-1 WHERE {1}gps_long = 'NA' or {1}gps_lat = 'NA';".format(year, col_prefix))
        print("ALTER TABLE lucas{0} ALTER COLUMN {1}gps_long TYPE {2} using {1}gps_long::{2};".format(year, col_prefix, "double precision"))
        print("ALTER TABLE lucas{0} ALTER COLUMN {1}gps_lat TYPE {2} using {1}gps_lat::{2};".format(year, col_prefix, "double precision"))
        if year <= 2018:
            print("UPDATE lucas{0} SET {1}gps_long = {1}gps_long * -1 FROM (SELECT ogc_fid as id from lucas{0} WHERE gps_ew = '2') as S WHERE ogc_fid = s.id;".format(year, col_prefix))
        print("ALTER TABLE lucas{} ADD COLUMN geog_gps geography(point, 4326);".format(year))
        print("UPDATE lucas{0} SET geog_gps = st_setsrid(st_geomfromtext('POINT(' || {1}gps_long || ' ' || {1}gps_lat || ')'), 4326)::geography WHERE {1}gps_proj = '1';".format(year, col_prefix))
        # geom_gps (3035)
        print("ALTER TABLE lucas{} ADD COLUMN geom_gps geometry(point, 3035);".format(year))
        print("UPDATE lucas{} SET geom_gps = st_transform(geog_gps::geometry, 3035);".format(year))
        # geom_thr (3035)
        print("ALTER TABLE lucas{} ADD COLUMN geom_thr geometry(point, 3035);".format(year))
        print("UPDATE lucas{} SET geom_thr = st_transform(geog_th::geometry, 3035);".format(year))
        print("UPDATE lucas{} SET geom_thr = st_setsrid(st_point((st_x(geom_thr) / 1000)::int * 1000, (st_y(geom_thr) / 1000)::int * 1000), 3035);".format(year))
        # geom (3035)
        print("ALTER TABLE lucas{} ADD COLUMN geom geometry (point, 3035);".format(year))
        print("UPDATE lucas{} SET geom = geom_gps WHERE geom_gps IS NOT NULL;".format(year))
        print("UPDATE lucas{} SET geom = geom_thr WHERE geom_gps IS NULL;".format(year))

        # geom_obs_radius (3035)
        print("ALTER TABLE lucas{} ADD COLUMN geom_obs_radius geometry(polygon, 3035);".format(year))
        if year == 2018:
            # compute OBS_RADIUS
            print("ALTER TABLE lucas{} ADD COLUMN obs_radius VARCHAR;".format(year))
            print("UPDATE lucas{0} AS a SET obs_radius = b.obs_radius FROM obs_radius_{0} AS b WHERE a.lc1 = b.lc1;".format(year))
        print("UPDATE lucas{} SET geom_obs_radius = st_buffer(geom_gps, 1.5) WHERE obs_radius = '1' AND geom_gps IS NOT NULL;".format(year))
        print("UPDATE lucas{} SET geom_obs_radius = st_buffer(geom_gps, 20) WHERE obs_radius = '2' AND geom_gps IS NOT NULL;".format(year))

        # geom_cprn_lc1nsew (3035)
        print("ALTER TABLE lucas{} ADD COLUMN geom_cprn_lc1nsew geometry(polygon, 3035);".format(year))
        if year == 2018:
            print("""UPDATE lucas{0} SET geom_cprn_lc1nsew = sq2.geom FROM (
SELECT point_id, st_union(array[
st_intersection(
 st_buffer(xy, {1}cprn_lc1n),
 st_polygon(st_makeline(array[
  st_point(x, y), st_point(x+dn, y+{1}cprn_lc1n), st_point(x-dn, y+{1}cprn_lc1n), st_point(x, y)]), 3035)
),
st_intersection(
 st_buffer(xy, {1}cprnc_lc1s),
 st_polygon(st_makeline(array[
  st_point(x, y), st_point(x+ds, y-{1}cprnc_lc1s), st_point(x-ds, y-{1}cprnc_lc1s), st_point(x, y)]), 3035)
),
st_intersection(
 st_buffer(xy, {1}cprnc_lc1e),
 st_polygon(st_makeline(array[
  st_point(x, y), st_point(x+{1}cprnc_lc1e, y+de), st_point(x+{1}cprnc_lc1e, y-de), st_point(x, y)]), 3035)
),
st_intersection(
 st_buffer(xy, {1}cprnc_lc1w),
 st_polygon(st_makeline(array[
  st_point(x, y), st_point(x-{1}cprnc_lc1w, y+dw), st_point(x-{1}cprnc_lc1w, y-dw), st_point(x, y)]), 3035)
) 
]) AS geom
FROM
(SELECT point_id, geom_gps AS xy, st_x(geom_gps) AS x, st_y(geom_gps) AS y,
  tan(pi()/4+pi()/180)*{1}cprn_lc1n::int AS dn,
  tan(pi()/4+pi()/180)*{1}cprnc_lc1s::int AS ds,
  tan(pi()/4+pi()/180)*{1}cprnc_lc1e::int AS de,
  tan(pi()/4+pi()/180)*{1}cprnc_lc1w::int AS dw,
  {1}cprnc_lc1w::int, {1}cprnc_lc1s::int, {1}cprn_lc1n::int, {1}cprnc_lc1e::int FROM data.lucas{0} WHERE geom_gps IS NOT NULL AND {1}cprn_lc1n NOT IN ('88', 'NA') AND {1}cprnc_lc1w::int > 0 AND {1}cprnc_lc1s::int > 0 AND {1}cprn_lc1n::int > 0 AND {1}cprnc_lc1e::int > 0) AS sq1
) AS sq2
WHERE lucas{0}.point_id = sq2.point_id;""".format(year, col_prefix))

        # compute distance attributes (obs_dist)
        print("UPDATE lucas{0} SET {1}obs_dist = NULL;".format(year, col_prefix))        
        print("UPDATE lucas{0} SET {1}obs_dist = round(st_distance(geom_thr, geom_gps)) "
              "WHERE geom_gps IS NOT NULL;".format(year, col_prefix))

        # create spatial indices
        print("CREATE index ON lucas{} USING gist (geog_gps);".format(year))
        print("CREATE index ON lucas{} USING gist (geom_gps);".format(year))
        print("CREATE index ON lucas{} USING gist (geom_thr);".format(year))
        print("CREATE index ON lucas{} USING gist (geom);".format(year))
        print("CREATE index ON lucas{} USING gist (geom_obs_radius);".format(year))
        print("CREATE index ON lucas{} USING gist (geom_cprn_lc1nsew);".format(year))

def remove_outliers(years):
    for year in years:
        print("""UPDATE lucas{y} SET geom_gps = NULL, geog_gps = NULL WHERE point_id IN (
WITH aoi AS (SELECT st_buffer(st_convexhull(st_collect(geom_thr)), 2000) AS geom FROM grid)
SELECT a.point_id FROM lucas{y} AS a LEFT JOIN aoi ON st_within(a.geom_gps, aoi.geom)
WHERE a.geom_gps IS NOT NULL AND aoi.geom IS NULL);""".format(y=year))

    print("DELETE FROM lucas{y} WHERE st_distance(st_transform(geog_th::geometry, 3035),geom_thr) > 10;".format(y=year))

if __name__ == "__main__":
    parser = Parser(csv_file=False, years=True)
    print("BEGIN;")

    create_geometries(parser.years)

    #
    # grid
    #
    # round coordinates (grid)
    print("""ALTER TABLE grid ADD COLUMN geom_thr geometry(point, 3035);
UPDATE grid SET geom_thr = st_transform(geog_th::geometry, 3035);
UPDATE grid SET geom_thr = st_setsrid(st_point((st_x(geom_thr) / 1000)::int * 1000, (st_y(geom_thr) / 1000)::int * 1000), 3035);"""
    )
    # re-type
    print("ALTER TABLE grid ALTER COLUMN point_id TYPE integer USING point_id::integer;")
    # create indices
    print("""CREATE INDEX on grid (point_id);
CREATE INDEX ON grid USING gist (geom_thr);"""
    )

    remove_outliers(parser.years)

    print("COMMIT")
