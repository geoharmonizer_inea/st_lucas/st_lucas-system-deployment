#!/usr/bin/env python3

import os

from lib.parser_cli import Parser
from lib.merge_points import MergePoints

class AggregateST(MergePoints):
    def __init__(self, table, columns, years):
        super().__init__(table, columns, years, exclude=["SPACE-TIME"])

    def _get_columns(self, year, target=True):
        cols = []
        for col, item in self.columns.items():
            if year in item["exclude"] or col in self.additional.keys():
                continue
            if target:
                cols.append(col if col in self.timeless else '{}_{}'.format(col, year))
            else:
                cols.append(col)

        return cols
    
    def build_sql(self):
        print("BEGIN;")
        print("CREATE TABLE {} (".format(self.table), end='')
        # unique columns
        print(','.join(map(lambda x: '{} {}'.format(x, self.columns[x]["dtype"]), self.timeless)), end=',')
        columns_all = []
        for col in self.columns.keys():
            if col in self.timeless or col in self.additional:
                continue
            for year in self.years:
                columns_all.append('{}_{} {}'.format(col, year, self.columns[col]["dtype"]))
        columns_all.append("survey_count integer")
        print(','.join(columns_all), end='')
        print(');')

        year = self.years[0]
        print("INSERT INTO {t} ({c1}) SELECT {c2} FROM lucas{y};".format(
            t=self.table, y=year,
            c1=','.join(self._get_columns(year, True)),
            c2=','.join(self._get_columns(year, False))
        ))

        key_column = "point_id"
        for year in self.years[1:]:
            # update existing points
            cols = self._get_columns(year, False)
            print("UPDATE {t} set {c3} "
                  "FROM (SELECT {c2} FROM lucas{y} WHERE {p} "
                  "IN (SELECT {p} FROM {t})) AS s WHERE {t}.{p}=s.{p};".format(
                      t=self.table, y=year,
                      c3=','.join(map(lambda x: '{x}_{y}=s.{x}'.format(x=x, y=year),
                                      set(cols).difference(self.timeless))),
                      c1=','.join(self._get_columns(year, True)),
                      c2=','.join(cols),
                      p=key_column
            ))
            # insert new points
            print("INSERT INTO {t} ({c1}) SELECT {c2} FROM lucas{y} WHERE NOT EXISTS "
                  "(SELECT 1 FROM {t} WHERE point_id = lucas{y}.point_id LIMIT 1);".format(
                      t=self.table, y=year,
                      c1=','.join(self._get_columns(year, True)),
                      c2=','.join(self._get_columns(year, False))
            ))

        # geom_gps (multipoint)
        print("ALTER TABLE {t} ADD COLUMN geom_gps geometry (multipoint, 3035);".format(
            t=self.table
        ))
        print("UPDATE {t} SET geom_gps = s.geom FROM (SELECT point_id, st_collect(array [".format(
            t=self.table), end=''
        )
        print(",".join(map(lambda x: f"geom_gps_{x}", self.years)), end='')
        print("]) AS geom FROM {t}) AS s WHERE {t}.point_id = s.point_id;".format(t=self.table))

        # geom
        print("ALTER TABLE {t} ADD COLUMN geom geometry (point, 3035);".format(
            t=self.table
        ))
        print("UPDATE {t} SET geom = s.geom FROM (SELECT point_id, st_centroid(geom_gps) "
              "AS geom FROM {t}) AS s WHERE {t}.point_id = s.point_id;".format(t=self.table))
        print("UPDATE {t} SET geom = geom_thr WHERE geom IS NULL;".format(t=self.table))

        # survey_dist
        for year in self.years:
            print("ALTER TABLE {t} ADD COLUMN survey_dist_{y} integer;".format(
                t=self.table, y=year
            ))
            print("UPDATE {t} SET survey_dist_{y} = -1 WHERE survey_date_{y} IS NOT NULL;".format(
                t=self.table, y=year
            ))
            print("UPDATE {t} SET survey_dist_{y} = s.dist FROM (SELECT point_id, "
                  "round(st_distance(geom, geom_gps_{y})) AS dist FROM {t} "
                  "WHERE geom_gps_{y} IS NOT NULL) AS s "
                  "WHERE {t}.point_id=s.point_id;".format(
                      t=self.table, y=year
            ))
        print("ALTER TABLE {t} ADD COLUMN survey_maxdist integer;".format(
            t=self.table, y=year
        ))
        print("UPDATE {t} SET survey_maxdist = greatest({c});".format(
            t=self.table, c=','.join(map(lambda x: 'survey_dist_{}'.format(x), self.years))
        ))

        # survey_count
        print("UPDATE {st} AS t SET survey_count=s.count FROM "
              "(SELECT point_id,count(*) AS count FROM {t} GROUP BY point_id) "
              "AS s WHERE t.point_id=s.point_id;".format(
                  st=self.table, t=os.environ['TABLE_NAME']
        ))

        # create indices
        self.create_indices(["point_id", "nuts0", "survey_date",
                             "geog_gps", "geom_gps", "geom_obs_radius", "geom_cprn_lc1nsew",
                             "geog_th", "geom_thr"]
        )
        print("CREATE INDEX ON {t} USING gist (geom_gps);".format(t=self.table))
        print("CREATE INDEX ON {t} USING gist (geom);".format(t=self.table))

        print("COMMIT;")

    def create_indices(self, columns):
        st_columns = []
        for col in columns:
            if col in self.timeless:
                st_columns.append(col)
            else:
                for year in self.years:
                    st_columns.append('{}_{}'.format(col, year))
        super().create_indices(st_columns)
        
        
if __name__ == "__main__":
    parser = Parser(
        args=[
            { 'dest': 'table', 'metavar': 'table', 'type': str,
              'help': 'Table name'
            }
        ]
    )

    my = AggregateST(parser.table, parser.columns,
                     parser.years
    )
    my.build_sql()
