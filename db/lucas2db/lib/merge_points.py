import os
import copy
from collections import OrderedDict

from osgeo import ogr

from abc import ABC, abstractmethod

class ColumnTypes:
    timeless = [
        "point_id",
        "nuts0",
        "geom_thr"
    ]
    additional = {
        "survey_year": {"dtype" : "integer", "exclude": []}
    }
    def __init__(self, columns, years, exclude=[]):
        if exclude:
            self.columns = OrderedDict()
            for col, meta in columns.items():
                if meta['group'].upper() in exclude:
                    continue
                self.columns[col] = meta
        else:
            self.columns = columns
        self.years = years

        self._collect_types()
        self._columns_additional(self.additional)

    def _columns_additional(self, columns):
        for col, meta in columns.items():
            if col in self.columns:
                self.columns[col].update(meta)

    @staticmethod
    def _gdal2pg(dtype):
        return {
            ogr.OFTInteger : "integer",
            ogr.OFTString  : "varchar",
            ogr.OFTReal    : "float",
            ogr.OFTDate    : "date"
        }[dtype]
        
    def _collect_types(self):
        for column in self.columns:
            self.columns[column]["dtype"] = None
            self.columns[column]["exclude"] = []
            if column.startswith("geog"):
                self.columns[column]["dtype"] = "geography(point)"
            elif column.startswith("geom"):
                self.columns[column]["dtype"] = "geometry({}, 3035)".format(
                    "polygon" if any(s in column for s in ("area", "radius", "cprn_lc1")) else "point"
                )

        # open connection to database
        ds = ogr.Open("PG:dbname={} user=postgres".format(os.environ["POSTGRES_DB"]))

        for year in self.years:
            table = '{}.lucas{}'.format(os.environ["POSTGRES_SCHEMA"], year)
            lyr = ds.GetLayer(table)
            lyr_defn = lyr.GetLayerDefn()
            for name in self.columns.keys():
                idx = lyr_defn.GetFieldIndex(name)
                if idx > -1:
                    self.columns[name]["dtype"] = \
                        self._gdal2pg(lyr_defn.GetFieldDefn(idx).GetType())
                elif not name.startswith(("geog", "geom")):
                    self.columns[name]["exclude"].append(year)

        # close connection
        ds = None
    
class MergePoints(ColumnTypes, ABC):
    def __init__(self, table, columns, years, exclude=None):
        super().__init__(columns, years, exclude)
        
        self.table = table

        print("SET search_path TO {},public;".format(os.environ["POSTGRES_SCHEMA"]))

    def create_indices(self, columns):
        for column in columns:
            sql = "CREATE INDEX ON {}".format(self.table)
            if column.startswith(("geom", "geog")):
                sql += " USING gist"
            sql += " ({});".format(column)
            print(sql)

    @abstractmethod
    def build_sql(self):
        pass
