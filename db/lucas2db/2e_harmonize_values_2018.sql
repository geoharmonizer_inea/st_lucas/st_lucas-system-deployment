--
-- 2018
--
ALTER TABLE lucas2018 ADD COLUMN lc1_perc_cls varchar;
UPDATE lucas2018 SET lc1_perc = '-1' WHERE lc1_perc::int=0 OR lc1_perc::int>100;
UPDATE lucas2018 SET lc1_perc_cls = '1' WHERE lc1_perc::int>=1 AND lc1_perc::int<10;
UPDATE lucas2018 SET lc1_perc_cls = '2' WHERE lc1_perc::int>=10 AND lc1_perc::int<25;
UPDATE lucas2018 SET lc1_perc_cls = '3' WHERE lc1_perc::int>=25 AND lc1_perc::int<50;
UPDATE lucas2018 SET lc1_perc_cls = '4' WHERE lc1_perc::int>=50 AND lc1_perc::int<75;
UPDATE lucas2018 SET lc1_perc_cls = '5' WHERE lc1_perc::int>=75 AND lc1_perc::int<=100;
UPDATE lucas2018 SET lc1_perc_cls = '-1' WHERE lc1_perc::int=-1;
ALTER TABLE lucas2018 ADD COLUMN lc2_perc_cls varchar;
UPDATE lucas2018 SET lc2_perc = '-1' WHERE lc2_perc::int=0 OR lc2_perc::int>100;
UPDATE lucas2018 SET lc2_perc_cls = '1' WHERE lc2_perc::int>=1 AND lc2_perc::int<10;
UPDATE lucas2018 SET lc2_perc_cls = '2' WHERE lc2_perc::int>=10 AND lc2_perc::int<25;
UPDATE lucas2018 SET lc2_perc_cls = '3' WHERE lc2_perc::int>=25 AND lc2_perc::int<50;
UPDATE lucas2018 SET lc2_perc_cls = '4' WHERE lc2_perc::int>=50 AND lc2_perc::int<75;
UPDATE lucas2018 SET lc2_perc_cls = '5' WHERE lc2_perc::int>=75 AND lc2_perc::int<=100;
UPDATE lucas2018 SET lc2_perc_cls = '-1' where lc2_perc::int=-1;
ALTER TABLE lucas2018 ADD COLUMN lu1_perc_cls varchar;
UPDATE lucas2018 SET lu1_perc = '-1' WHERE lu1_perc::int=0 OR lu1_perc::int>100;
UPDATE lucas2018 SET lu1_perc_cls = '1' WHERE lu1_perc::int>=1 AND lu1_perc::int<5;
UPDATE lucas2018 SET lu1_perc_cls = '2' WHERE lu1_perc::int>=5 AND lu1_perc::int<10;
UPDATE lucas2018 SET lu1_perc_cls = '3' WHERE lu1_perc::int>=10 AND lu1_perc::int<25;
UPDATE lucas2018 SET lu1_perc_cls = '4' WHERE lu1_perc::int>=25 AND lu1_perc::int<50;
UPDATE lucas2018 SET lu1_perc_cls = '5' WHERE lu1_perc::int>=50 AND lu1_perc::int<75;
UPDATE lucas2018 SET lu1_perc_cls = '6' WHERE lu1_perc::int>=75 AND lu1_perc::int<90;
UPDATE lucas2018 SET lu1_perc_cls = '7' WHERE lu1_perc::int>=90 AND lu1_perc::int<=100;
UPDATE lucas2018 SET lu1_perc_cls = '-1' WHERE lu1_perc::int=-1;
ALTER TABLE lucas2018 ADD COLUMN lu2_perc_cls varchar;
UPDATE lucas2018 SET lu2_perc = '-1' WHERE lu2_perc::int=0 OR lu2_perc::int>100;
UPDATE lucas2018 SET lu2_perc_cls = '1' WHERE lu2_perc::int>=1 AND lu2_perc::int<5;
UPDATE lucas2018 SET lu2_perc_cls = '2' WHERE lu2_perc::int>=5 AND lu2_perc::int<10;
UPDATE lucas2018 SET lu2_perc_cls = '3' WHERE lu2_perc::int>=10 AND lu2_perc::int<25;
UPDATE lucas2018 SET lu2_perc_cls = '4' WHERE lu2_perc::int>=25 AND lu2_perc::int<50;
UPDATE lucas2018 SET lu2_perc_cls = '5' WHERE lu2_perc::int>=50 AND lu2_perc::int<75;
UPDATE lucas2018 SET lu2_perc_cls = '6' WHERE lu2_perc::int>=75 AND lu2_perc::int<90;
UPDATE lucas2018 SET lu2_perc_cls = '7' WHERE lu2_perc::int>=90 AND lu2_perc::int<=100;
UPDATE lucas2018 SET lu2_perc_cls = '-1' WHERE lu2_perc::int=-1;
ALTER TABLE lucas2018 ADD COLUMN soil_stones_perc_cls varchar;
UPDATE lucas2018 SET soil_stones_perc = '-1' WHERE soil_stones_perc::int>100;
UPDATE lucas2018 SET soil_stones_perc_cls = '1' WHERE soil_stones_perc::int>=0 AND soil_stones_perc::int<10;
UPDATE lucas2018 SET soil_stones_perc_cls = '2' WHERE soil_stones_perc::int>=10 AND soil_stones_perc::int<25;
UPDATE lucas2018 SET soil_stones_perc_cls = '3' WHERE soil_stones_perc::int>=25 AND soil_stones_perc::int<50;
UPDATE lucas2018 SET soil_stones_perc_cls = '4' WHERE soil_stones_perc::int>=50 AND soil_stones_perc::int<=100;
UPDATE lucas2018 SET soil_stones_perc_cls = '-1' WHERE soil_stones_perc::int=-1;
UPDATE lucas2018 SET parcel_area_ha = '1' WHERE parcel_area_ha = '2';
UPDATE lucas2018 SET parcel_area_ha = '2' WHERE parcel_area_ha = '3';
UPDATE lucas2018 SET parcel_area_ha = '3' WHERE parcel_area_ha = '4';
UPDATE lucas2018 SET parcel_area_ha = '4' WHERE parcel_area_ha = '5';

-- lc1_spec
UPDATE lucas2018 SET lc1_spec = '-1' WHERE lc1_spec = 'B37Z';
UPDATE lucas2018 SET lc1_spec = '-1' WHERE lc1_spec = 'B43Z';
UPDATE lucas2018 SET lc1_spec = '-1' WHERE lc1_spec = 'B53Z';
UPDATE lucas2018 SET lc1_spec = '-1' WHERE lc1_spec = 'B75Z';
UPDATE lucas2018 SET lc1_spec = '-1' WHERE lc1_spec = 'B77Z';
UPDATE lucas2018 SET lc1_spec = '-1' WHERE lc1_spec = 'B83Z';
UPDATE lucas2018 SET lc1_spec = '-1' WHERE lc1_spec = 'C10Z';
UPDATE lucas2018 SET lc1_spec = '-1' WHERE lc1_spec = 'C21Z';
UPDATE lucas2018 SET lc1_spec = '-1' WHERE lc1_spec = 'C22Z';
UPDATE lucas2018 SET lc1_spec = '-1' WHERE lc1_spec = 'C23Z';
UPDATE lucas2018 SET lc1_spec = '-1' WHERE lc1_spec = 'C31Z';
UPDATE lucas2018 SET lc1_spec = '-1' WHERE lc1_spec = 'C32Z';
UPDATE lucas2018 SET lc1_spec = '-1' WHERE lc1_spec = 'C33Z';

--
-- lc2_spec
UPDATE lucas2018 SET lc2_spec = '-1' WHERE lc2_spec = 'C10Z';

--
-- soil_taken
ALTER TABLE lucas2018 ADD COLUMN soil_taken varchar;
UPDATE lucas2018 SET soil_taken = '1' WHERE soil_blk_0_10_taken = '1' OR soil_blk_10_20_taken = '1' OR soil_blk_20_30_taken = '1';

--
-- No LC harmonization applied
--

--
-- No LU harmonization applied
--
