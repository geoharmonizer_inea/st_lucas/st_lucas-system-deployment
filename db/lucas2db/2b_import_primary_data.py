#!/usr/bin/env python3

import os
import sys
import argparse
import re
import tempfile
from pathlib import Path

from osgeo import gdal

gdal.UseExceptions()

def import_points(data_dir, dsn_out):
    ds_out = gdal.OpenEx(dsn_out, gdal.OF_VECTOR | gdal.OF_UPDATE)
        
    for f_path in Path(data_dir).glob('*.[cC][sS][vV]'):
        m = re.search("([0-9]{4})", str(f_path))
        year = int(m.group(1))

        with open(f_path, 'r') as fd:
            lines = fd.readlines()
            if 'rows selected' in lines[-2]:
                # remove last three lines when 'rows selected' detected
                
                f_path = Path(tempfile.gettempdir(), f_path.name)
                with open(f_path, "w") as tmp:
                    tmp.writelines(lines[:-3])

        table = f"{os.environ['POSTGRES_SCHEMA']}.lucas{year}"
        print(f"Importing {f_path.stem} -> {table}...", file=sys.stderr)

        if ds_out.GetLayerByName(table) is None:
            kwargs = {
                'accessMode': None,
                'layerCreationOptions': ["GEOMETRY_NAME=geog_th", "GEOM_TYPE=geography"]
            }
        else:
            kwargs = {
                'accessMode': 'append'
            }
        if year == 2006:
            x = "x_laea"
            y = "y_laea"
            kwargs['srcSRS'] = "EPSG:3035"
            kwargs['dstSRS'] = "EPSG:4326"
        elif year == 2022:
            x = "point_long"
            y = "point_lat"
        else:
            x = "th_long"
            y = "th_lat"
            
        ds_in = gdal.OpenEx(str(f_path), gdal.OF_VECTOR | gdal.OF_READONLY,
                            open_options=[f"X_POSSIBLE_NAMES={x}", f"Y_POSSIBLE_NAMES={y}"]
        )
        gdal.VectorTranslate(dsn_out, ds_in, format='PostgreSQL', layerName=table,
                             **kwargs
        )
        ds_in = None

    ds_out = None

def import_grid(data_dir, dsn_out):
    for f_path in Path(data_dir).glob('*.[cC][sS][vV]'):
        table = f"{os.environ['POSTGRES_SCHEMA']}.grid"
        print(f"Importing {f_path.stem} -> {table}...", file=sys.stderr)
        ds_in = gdal.OpenEx(str(f_path), gdal.OF_VECTOR | gdal.OF_READONLY,
                            open_options=["X_POSSIBLE_NAMES=X_WGS84", "Y_POSSIBLE_NAMES=Y_WGS84",
                                          "AUTODETECT_TYPE=YES"]
        )
        gdal.VectorTranslate(dsn_out, ds_in, format='PostgreSQL',
                             layerName=table,
                             accessMode="overwrite",
                             layerCreationOptions=["GEOMETRY_NAME=geog_th", "GEOM_TYPE=geography",
                                                   "FID=point_id"]
        )
        ds_in = None

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('data_dir', metavar='data_dir', type=str,
                        help='Path to data directory')

    args = parser.parse_args()

    dsn_out = f"PG:dbname={os.environ['POSTGRES_DB']}"    
    import_points(Path(args.data_dir, 'points'), dsn_out)
    import_grid(Path(args.data_dir, 'grid'), dsn_out)
