import pytest
import os
import csv
import requests
import sys
# import warnings
import json
import re
from pathlib import Path

from lib.parser_cli import read_csv, read_groups
from utils import Db

class Test2(Db):
    years = list(map(int, os.environ["YEARS"].split(',')))
    csv_dir = Path(os.environ["DATA_DIR"], "primary", "points")
    grid = Path(os.environ["DATA_DIR"], "primary", "grid", "GRID_CSVEXP_20171113.csv")

    code_dir = os.environ["CODE_DIR"]
    
    def _csv_rows(self, year):
        skip = 4 if year in (2009, 2012, 2015, 2018) else 1
        row_sum = 0
        for f_path in Path(self.csv_dir).glob('*.[cC][sS][vV]'):
            if str(year) in str(f_path):
                with open(f_path) as f:
                    reader = csv.reader(f)
                    row_sum += len(list(reader)) - skip  # header + footer
        return row_sum

    def _table_cols(self, schema_name, table_name):
        with self._connect() as conn:
            with conn.cursor() as cur:
                tbl_cols = self._fetchall(
                    cur,
                    f"SELECT column_name FROM information_schema.columns WHERE table_schema = "
                    f"'{schema_name}' AND table_name = '{table_name}'"
                )
            
        return self._rows2list(tbl_cols)

    @staticmethod
    def _groups():
        """Parse groups.

        :return list: list of groups
        """
        return read_groups(
            os.path.join(os.environ['CONF_DIR'], 'thematic_attributes.json')
        )

    def test_2a_001(self):
        """DB initialization.

        This tests case consists of checking that DB schemas were
        created according to the configuration.
        """
        with self._connect() as conn:
            with conn.cursor() as cur:
                for schema in (os.environ["POSTGRES_SCHEMA"],
                               os.environ["MAPSERVR_SCHEMA"]):
                    exists = self._fetchone(
                        cur,
                        "SELECT EXISTS(SELECT 1 FROM information_schema.schemata "
                        f"WHERE schema_name = '{schema}')"
                    )
                    assert exists == True

    def test_2b_001(self):
        """Primary data import.

        This tests case consists of checking that for each year a
        number of rows in input CSV file is the same as a number of
        rows in DB table.
        """
        with self._connect() as conn:
            with conn.cursor() as cur:
                for year in self.years:
                    count = self._fetchone(
                        cur,
                        f"SELECT count(*) FROM {self.data_schema}.lucas{year}"
                    )
                    assert count == self._csv_rows(year), year

    def test_2b_002(self):
        """Primary data import.

        This tests case consists of checking that for each year the
        first row in input CSV file is the same as corresponding
        record in DB table.
        """
        with self._connect() as conn:
            with conn.cursor() as cur:
                # skip if deployment is already finished
                views = self._rows2list(
                    self._fetchall(
                        cur,
                        f"SELECT viewname FROM pg_catalog.pg_views WHERE schemaname = '{self.ms_schema}'"
                ))
                if len(views) > 0:
                    return

                for f_path in Path(self.csv_dir).glob('*.[cC][sS][vV]'):
                    m = re.search("([0-9]{4})", str(f_path))
                    year = m.group(1)
                    with open(f_path) as f:
                        reader = csv.reader(f, delimiter=',')
                        header_line = next(reader)
                        first_line = next(reader)
                        idx = list(map(lambda x: x.lower(), header_line)).index('point_id')
                        row = list(self._fetchall(
                            cur,
                            f"SELECT * FROM {self.data_schema}.lucas{year} WHERE point_id = '{first_line[idx]}'"
                        )[0])[1:-1] # skip ogc_fid, geom
                        assert len(row) == len(first_line) # skip ogc_fid + geom
                        i = 0
                        for item in row:
                            assert item == row[i]
                            i += 1

    def test_2b_003(self):
        """Primary data import.

        This tests case consists of checking that a number of rows in
        input grid CSV file is the same as a number of rows in DB
        table.
        """
        def _csv_grid_rows(grid_csv):
            with open(grid_csv) as g:
                reader = csv.reader(g)
                return len(list(reader)) - 1  # header

        with self._connect() as conn:
            with conn.cursor() as cur:
                count = self._fetchone(
                    cur,
                    f"SELECT count(*) FROM {self.data_schema}.grid"
                )
                assert count == _csv_grid_rows(self.grid)

    def test_2c_001(self):
        """Coordinates harmonization.

        This tests case consists of checking that for each year
        geometry and geography attributes were created.
        """
        def _column_attr(tbl, column_name):
            return self._fetchall(
                cur,
                f"SELECT f_{column_name}_column,srid,type FROM {column_name}_columns "
                f"WHERE f_table_schema = '{self.data_schema}' AND f_table_name = '{tbl}'"
            )

        with self._connect() as conn:
            with conn.cursor() as cur:
                for year in self.years:
                    table = f"lucas{year}"
                    sql_geog = _column_attr(table, "geography")
                    assert ("geog_th", 4326, "Point") in sql_geog
                    assert ("geog_gps", 4326, "Point") in sql_geog

                    sql_geom = _column_attr(table, "geometry")
                    assert ("geom_gps", 3035, "POINT") in sql_geom
                    assert ("geom_thr", 3035, "POINT") in sql_geom
                    assert ("geom_obs_radius", 3035, "POLYGON") in sql_geom
                    if year == 2018:
                        assert ("geom_cprn_lc1nsew", 3035, "POLYGON") in sql_geom

    def test_2c_002(self):
        """Coordinates harmonization.

        This tests case consists of checking that for each year all
        records have harmonized geometry defined.
        """
        with self._connect() as conn:
            with conn.cursor() as cur:
                for year in self.years:
                    table = f"lucas{year}"
                    count = self._fetchone(
                        cur,
                        f"SELECT count(*) FROM {self.data_schema}.{table} "
                        f"WHERE geom IS NULL AND st_isempty(geom) = false"
                    )
                    assert count == 0

    def test_2d_001(self):
        """Attributes harmonization.

        This tests case consists of checking that for each year all
        attributes were renamed according to the system configuration.
        """
        cols = read_csv(
            os.path.join(os.environ['CONF_DIR'], 'harmonize_attributes.csv')
        )
        for year in self.years:
            table_cols = self._table_cols(
                self.data_schema, f'lucas{year}'
            )

            for col_name in table_cols:
                assert col_name not in cols, year

    def test_2e_001(self):
        """Data values harmonization.

        This tests case consists of checking that for each year
        DB attributes take only values from the predefined
        domains.
        """
        def get_nuts(url, level):
            nuts_get = requests.get(url)
            nuts_json = nuts_get.json()
            nuts_all = list(nuts_json.keys())
            nuts_units = []
            for unit in nuts_all:
                if len(unit) == level + 2:
                    nuts_units.append(unit.lower())
            return nuts_units

        cols = read_csv(
            os.path.join(os.environ['CONF_DIR'], 'list_of_attributes.csv')
        )
        with self._connect() as conn:
            with conn.cursor() as cur:
                count = len(cols)
                i = 0
                for col_name, item in cols.items():
                    i += 1
                    # print("Checking {} of {} ({}){}".format(i, count, col_name, ' '*20), end='\r', flush=True)
                    if col_name.startswith('geo') or item['group'] == '' or item['domain'] == '':
                        continue

                    values = None
                    if col_name.startswith('nuts') is False:
                        values = []
                        for val in item['domain'].split(', '):
                            values.append(val)
                    for year in self.years:
                        # check if column exists in the table
                        table_cols = self._table_cols(
                            self.data_schema, f'lucas{year}'
                        )
                        if col_name not in table_cols:
                            continue

                        base_url = 'https://gisco-services.ec.europa.eu/distribution/v2/nuts'
                        if col_name.startswith('nuts'):
                            nuts_year = year
                            nuts_url = f'{base_url}/nuts-{nuts_year}-units.json'
                            while requests.get(nuts_url).status_code != 200:
                                nuts_year -= 1
                                nuts_url = f'{base_url}/nuts-{nuts_year}-units.json'
                            nuts_level = int(col_name[4])
                            values = get_nuts(nuts_url, nuts_level)

                        attr_vals = self._fetchall(
                            cur,
                            f"SELECT distinct({col_name}) FROM {self.data_schema}.lucas{year}"
                        )

                        for tbl_val in attr_vals:
                            if tbl_val[0] is None:
                                # skip NULL values
                                continue
                            try:
                                if '0-100' in values or '1-100' in values:
                                    assert year is not None and col_name is not None and \
                                        (str(tbl_val[0]).lower() in values or (int(tbl_val[0]) >= 0 and int(tbl_val[0]) <= 100))
                                else:
                                    assert year is not None and col_name is not None and \
                                        (str(tbl_val[0]).lower() in values)
                            except AssertionError as e:
                                # warnings.warn(UserWarning(e))
                                pass

                        # check l3_missing_level attributes
                        attr_vals = self._fetchall(
                            cur,
                            f"SELECT lc1_h_l3_missing, lc1_h_l3_missing_level, lc2_h_l3_missing, lc2_h_l3_missing_level "
                            f"FROM {self.data_schema}.lucas{year}"
                        )
                        for tbl_val in attr_vals:
                            if tbl_val[0] is not None:
                                if tbl_val[0].endswith('00'):
                                    assert tbl_val[1] == 1
                                else:
                                    assert tbl_val[1] == 2
                            if tbl_val[2] is not None:
                                if tbl_val[2].endswith('00'):
                                    assert tbl_val[3] == 1
                                else:
                                    assert tbl_val[3] == 2

    def test_2e_002(self):
        """Attributes harmonization.

        This tests case consists of checking office_pi/obs_type
        consistency in 2018.
        """
        with self._connect() as conn:
            with conn.cursor() as cur:
                count = self._fetchone(
                    cur,
                    f"select count(*) from data.lucas2018 where obs_type = '7' and office_pi != '1'"
                )
                assert count == 0

    def test_2e_003(self):
        """Attributes harmonization.

        This tests case consists of checking coding tables
        consistency.
        """
        # reader codes
        columns = {}
        for filename in os.listdir(self.code_dir):
            with open(os.path.join(self.code_dir, filename)) as fd:
                col_name = os.path.splitext(filename)[0]
                reader = csv.reader(fd, delimiter=';')
                next(reader) # skip header
                codes = []
                for row in reader:
                    codes.append(row[0])
            columns[col_name] = codes

        # check consistency with DB
        with self._connect() as conn:
            with conn.cursor() as cur:
                for year in self.years:
                    table_cols = self._table_cols(
                        self.data_schema, f'lucas{year}'
                    )
                    for col_name in columns:
                        # check if column exists in the table
                        if col_name not in table_cols:
                            continue

                        col_vals = self._fetchall(
                            cur,
                            f"SELECT distinct({col_name}) FROM {self.data_schema}.lucas{year}"
                        )
                        for val in col_vals:
                            assert col_name, val[0] is None or val[0] in columns[col_name]

    def test_2f_001(self):
        """Data types harmonization.

        This tests case consists of checking that for each year
        data types are defined according to the system configuration.
        """
        def _type_comp(col_name, dtype1, dtype2):
            return dtype1 == dtype2 or (dtype1 == "float" and dtype2 == "double precision")

        cols = read_csv(
            os.path.join(os.environ['CONF_DIR'], 'harmonize_data_types.csv')
        )

        with self._connect() as conn:
            with conn.cursor() as cur:
                for year in self.years:
                    tbl_cols = self._fetchall(
                        cur,
                        f"SELECT column_name, data_type FROM information_schema.columns WHERE table_schema = "
                        f"'{self.data_schema}' AND table_name = 'lucas{year}'"
                    )

                    for col_name, item in cols.items():
                        col_type = item['data_type']
                        for tbl_col_name, tbl_col_type in tbl_cols:
                            if col_name != tbl_col_name:
                                continue
                            assert _type_comp(col_name, col_type, tbl_col_type)

    def test_2g_001(self):
        """Merge harmonized data.

        This tests case consists of checking that total number of rows
        in primary CSV files for all years is the same as a number of
        records in merged DB table.
        """
        row_sum = 0
        for year in self.years:
            row_sum += self._csv_rows(year)

        with self._connect() as conn:
            with conn.cursor() as cur:
                count = self._fetchone(
                    cur,
                    f"SELECT count(*) FROM {self.data_schema}.{os.environ['TABLE_NAME']}"
                )
                assert count == row_sum

    def test_2g_002(self):
        """Merge harmonized data.

        This tests case consists of checking that for each year a
        number of records in DB tables is the same as a number of
        records in merged DB table where 'survey_year' defines current
        year.
        """
        with self._connect() as conn:
            with conn.cursor() as cur:
                for year in self.years:
                    table = f"lucas{year}"
                    count_table = self._fetchone(
                        cur,
                        f"SELECT count(*) FROM {self.data_schema}.{table}"
                    )
                    survey_count = self._fetchone(
                        cur,
                        f"SELECT count(*) FROM {self.data_schema}.{os.environ['TABLE_NAME']} WHERE survey_year = {year}"
                    )
                    assert count_table == survey_count

    def test_2g_003(self):
        """Merge harmonized data.

        This tests case consists of checking that attributes in merged
        DB table are defined according to the system configuration.
        """
        cols = read_csv(
            os.path.join(os.environ['CONF_DIR'], 'list_of_attributes.csv')
        )

        with self._connect() as conn:
            with conn.cursor() as cur:
                tbl_attrs = self._fetchall(
                    cur,
                    f"SELECT column_name, data_type FROM information_schema.columns WHERE table_schema = "
                    f"'{self.data_schema}' AND table_name = '{os.environ['TABLE_NAME']}'"
                )[1:] # skip 'fid'

                i = 0
                for col_name, item in cols.items():
                    if 'space-time' in item['group']:
                        continue
                    assert col_name == tbl_attrs[i][0]
                    i += 1

    def test_2g_004(self):
        """Merge harmonized data.

        This tests case consists of checking that all records in
        merged DB table have harmonized geometry defined.
        """
        with self._connect() as conn:
            with conn.cursor() as cur:
                count = self._fetchone(
                    cur,
                    f"SELECT count(*) FROM {self.data_schema}.{os.environ['TABLE_NAME']} "
                    f"WHERE geom IS NULL AND st_isempty(geom) = false"
                )
                assert count == 0

    def test_2h_001(self):
        """Space-time aggregation.

        This tests case consists of checking that survey_year
        attribute is defined for each year.
        """
        with self._connect() as conn:
            with conn.cursor() as cur:
                attrs = self._fetchall(
                    cur,
                    f"SELECT column_name FROM information_schema.columns "
                    f"WHERE table_name = '{os.environ['TABLE_NAME_ST']}' AND table_schema = '{self.data_schema}'"
                )
                for year in self.years:
                    assert (f"survey_date_{year}",) in attrs

    def test_2h_002(self):
        """Space-time aggregation.

        This tests case consists of checking that sum of survey_count
        attribute values is the same as a number of records in merged
        DB table.
        """
        with self._connect() as conn:
            with conn.cursor() as cur:
                sum_survey_count = self._fetchone(
                    cur,
                    f"SELECT sum(survey_count) FROM {self.data_schema}.{os.environ['TABLE_NAME_ST']}"
                )
                count_all = self._fetchone(
                    cur,
                    f"SELECT count(*) FROM {self.data_schema}.{os.environ['TABLE_NAME']}"
                )
                assert sum_survey_count == count_all

    def test_2h_003(self):
        """Space-time aggregation.

        This tests case consists of checking that all records in
        aggregated DB table have harmonized geometry defined.
        """
        with self._connect() as conn:
            with conn.cursor() as cur:
                count = self._fetchone(
                    cur,
                    f"SELECT count(*) FROM {self.data_schema}.{os.environ['TABLE_NAME_ST']} "
                    f"WHERE geom IS NULL AND st_isempty(geom) = false"
                )
                assert count == 0

    def test_2i_001(self):
        """Publication views.

        This tests case consists of checking that all group-based
        views were created in Db.
        """
        with self._connect() as conn:
            with conn.cursor() as cur:
                views = self._rows2list(
                    self._fetchall(
                        cur,
                        f"SELECT viewname FROM pg_catalog.pg_views WHERE schemaname = '{self.ms_schema}'"
                ))

        groups = self._groups().keys()
        assert len(groups) * 4 == len(views) - 2 # skip TABLE_NAME, TABLE_NAME_ST
        assert os.environ["TABLE_NAME"] in views
        assert os.environ["TABLE_NAME_ST"] in views
        for name in groups:
            assert f"{os.environ['TABLE_NAME']}_{name}" in views
            assert f"{os.environ['TABLE_NAME_ST']}_{name}" in views
            assert f"{os.environ['TABLE_NAME']}_{name}_base" in views
            assert f"{os.environ['TABLE_NAME_ST']}_{name}_base" in views

    def test_2i_002(self):
        """Publication views.

        This tests case consists of checking that attributes of
        group-based DB views are defined according to the system
        configuration.
        """
        def comp_attrs(table, st=False, group=None, base=False):
            skip = 0 if st else 1
            tbl_attrs = self._fetchall(
                cur,
                f"SELECT column_name, data_type FROM information_schema.columns WHERE table_schema = "
                f"'{self.ms_schema}' AND table_name = '{table}'"
                )[skip:]

            i = 0
            for col_name, item in cols.items():
                if item['group'] == '' or col_name.startswith('geo'):
                    continue
                if group is not None and \
                   item['group'] != 'default' and \
                   item['group'] not in group[1]:
                    continue
                if st is False and 'space-time' in item['group']:
                    continue
                if st is True and col_name == 'survey_year':
                    continue
                if base and item['base'] == '0':
                    continue
                if st and col_name not in ('point_id', 'nuts0', 'survey_count', 'survey_maxdist'):
                    for year in self.years:
                        assert f"{col_name}_{year}" == tbl_attrs[i][0]
                        i += 1
                else:
                    assert col_name == tbl_attrs[i][0]
                    i += 1

        cols = read_csv(
            os.path.join(os.environ['CONF_DIR'], 'list_of_attributes.csv')
        )

        with self._connect() as conn:
            with conn.cursor() as cur:
                groups = self._groups()
                comp_attrs(os.environ['TABLE_NAME'])
                comp_attrs(os.environ['TABLE_NAME_ST'], True)
                for name, desc in groups.items():
                    comp_attrs(f"{os.environ['TABLE_NAME']}_{name}", group=(name, desc))
                    comp_attrs(f"{os.environ['TABLE_NAME']}_{name}_base", group=(name, desc), base=True)
                    comp_attrs(f"{os.environ['TABLE_NAME_ST']}_{name}", True, group=(name, desc))
                    comp_attrs(f"{os.environ['TABLE_NAME_ST']}_{name}_base", True, group=(name, desc), base=True)

    def test_2i_003(self):
        """Publication views.

        This tests case consists of checking that number of records in
        DB tables located in data and ms schemas is the same.
        """
        def _sql_count(schema, table):
            return self._fetchone(
                cur,
                f"SELECT count(*) FROM {schema}.{table}"
            )

        with self._connect() as conn:
            with conn.cursor() as cur:
                for table in (os.environ["TABLE_NAME"], os.environ["TABLE_NAME_ST"]):
                    assert _sql_count(self.data_schema, table) == _sql_count(self.ms_schema, table)

    def test_2i_004(self):
        """Publication views.

        This tests case consists of checking that all records in DB
        views have harmonized geometry defined.
        """
        with self._connect() as conn:
            with conn.cursor() as cur:
                for table in (os.environ['TABLE_NAME'], os.environ['TABLE_NAME_ST']):
                    count = self._fetchone(
                        cur,
                        f"SELECT count(*) FROM {self.data_schema}.{table} "
                        f"WHERE geom IS NULL AND st_isempty(geom) = false"
                    )
                    assert count == 0

    def test_2j_001(self):
        """Publication views.

        This tests case consists of checking that Db recovery file was created.
        """
        dump_file = os.path.join(os.environ["DATA_DIR"], "dump.sql.7z")
        assert os.path.exists(dump_file) and os.path.isfile(dump_file)
