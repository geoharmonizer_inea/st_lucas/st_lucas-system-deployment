#!/usr/bin/env python3

import os
import argparse
import re

def scan_dir(data_dir):
    years = set()
    for f in os.listdir(data_dir):
       m = re.search("([0-9]{4})", f)
       years.add(int(m.group(1)))

    if os.environ.get("YEARS") is None:
        print("YEARS={}".format(','.join(map(str, sorted(years)))))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('data_dir', metavar='data_dir', type=str,
                        help='Path to data directory')

    args = parser.parse_args()

    scan_dir(args.data_dir)
