import os
import psycopg2

class Db:
    dbname = os.environ["POSTGRES_DB"]
    port = os.getenv("POSTGRES_PORT", 5432)
    user = "postgres"
    password = os.environ["POSTGRES_PASSWORD"]
    host = os.getenv("POSTGRES_HOST")
    ms_schema = os.environ["MAPSERVR_SCHEMA"]
    data_schema = os.environ["POSTGRES_SCHEMA"]

    def _connect(self):
        conn_string = f"dbname={self.dbname}"
        conn_string += f" port='{self.port}'"
        conn_string += f" user='{self.user}'"
        conn_string += f" password='{self.password}'"
        if self.host:
            conn_string += f" host='{self.host}'"

        return psycopg2.connect(conn_string)

    @staticmethod
    def _fetchone(cur, sql):
        cur.execute(sql)
        res = cur.fetchone()
        return res[0]

    @staticmethod
    def _fetchall(cur, sql):
        cur.execute(sql)
        return cur.fetchall()

    @staticmethod
    def _rows2list(rows):
        return [item for t in rows for item in t]

def WS():
    from owslib.wfs import WebFeatureService

    return WebFeatureService(
        url=f'{os.environ["HOSTNAME"]}/geoserver/wfs',
        version='1.1.0'
    )
