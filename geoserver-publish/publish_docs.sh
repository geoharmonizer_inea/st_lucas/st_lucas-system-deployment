#!/bin/sh -e

# build main docs
cd /opt/docs
rm -rf tables && mkdir tables
python3 utils/list_of_attributes.py /opt/conf/list_of_attributes.csv /opt/conf/thematic_attributes.json tables_csv/coding/ tables_csv/
python3 utils/csv2rst.py tables_csv/ tables/
make clean
make html

# build Python API docs
cd /opt/st_lucas-python-package/docs
git pull
make html
cp -r _build/html /opt/sphinx/build/html/api

# build QGIS plugin docs
cd /opt/st_lucas-qgis-plugin/help
git pull
make html
cp -r build/html /opt/sphinx/build/html/qgis_plugin

exit 0
