--
-- 2012
--
--UPDATE lucas2012 SET survey_date = -1 WHERE survey_date = '0001-01-01';
UPDATE lucas2012 SET lc1_perc = '1' WHERE lc1_perc = '2';
UPDATE lucas2012 SET lc1_perc = '2' WHERE lc1_perc = '3';
UPDATE lucas2012 SET lc1_perc = '3' WHERE lc1_perc = '4';
UPDATE lucas2012 SET lc1_perc = '4' WHERE lc1_perc = '5';
UPDATE lucas2012 SET lc1_perc = '5' WHERE lc1_perc = '6';
UPDATE lucas2012 SET lc2_perc = '1' WHERE lc2_perc = '2';
UPDATE lucas2012 SET lc2_perc = '2' WHERE lc2_perc = '3';
UPDATE lucas2012 SET lc2_perc = '3' WHERE lc2_perc = '4';
UPDATE lucas2012 SET lc2_perc = '4' WHERE lc2_perc = '5';
UPDATE lucas2012 SET lc2_perc = '5' WHERE lc2_perc = '6';
UPDATE lucas2012 SET lc_lu_special_remark = '22' WHERE lc_lu_special_remark = '1';
UPDATE lucas2012 SET lc_lu_special_remark = '1' WHERE lc_lu_special_remark = '2';
UPDATE lucas2012 SET lc_lu_special_remark = '2' WHERE lc_lu_special_remark = '22';
UPDATE lucas2012 SET lc_lu_special_remark = '10' WHERE lc_lu_special_remark = '7';
UPDATE lucas2012 SET crop_residues = '22' WHERE crop_residues = '1';
UPDATE lucas2012 SET crop_residues = '1' WHERE crop_residues in ('2','3','4');
UPDATE lucas2012 SET crop_residues = '2' WHERE crop_residues = '22';
UPDATE lucas2012 SET lc1_spec = 'B53B' WHERE lc1_spec = 'B53D';
UPDATE lucas2012 SET lc1_h = 'B23' WHERE lc1_spec = 'B43D';
UPDATE lucas2012 SET lc1_spec = 'B23N' WHERE lc1_spec = 'B43D';
UPDATE lucas2012 SET lc2_spec = 'B53B' WHERE lc2_spec = 'B53D';
UPDATE lucas2012 SET lc1_spec = '-1' WHERE lc1_spec = 'B77J';
UPDATE lucas2012 SET lc2_h = 'B23' WHERE lc2_spec = 'B43D';
UPDATE lucas2012 SET lc2_spec = 'B23N' WHERE lc2_spec = 'B43D';

--
-- LC harmonization
--
-- LC1
UPDATE lucas2012 SET lc1_h = '-1' WHERE lc1 IN ('G10', 'G20');
UPDATE lucas2012 SET lc1_h = 'B16' WHERE lc1_spec = 'B19E';
--
UPDATE lucas2012 SET lc1_spec = '-1' WHERE lc1_spec = 'B19E';
--
-- LC2
UPDATE lucas2012 SET lc2_h = '-1' WHERE lc2 IN ('G10', 'G20');

--
-- LU harmonization
--
-- LU1
UPDATE lucas2012 SET lu1_h = '-1' WHERE lu1 IN ('U340', 'U410');
UPDATE lucas2012 SET lu1_h = 'U341' WHERE lu1 = 'U363';
-- LU2
UPDATE lucas2012 SET lu2_h = '-1' WHERE lu2 IN ('U410', 'U340');
UPDATE lucas2012 SET lu2_h = 'U341' WHERE lu2 = 'U363';

--
-- Missing translation to level 3
--
-- LC1
UPDATE lucas2012 SET lc1_h_l3_missing = 'G10', lc1_h_l3_missing_level = 2 WHERE lc1 = 'G10';
UPDATE lucas2012 SET lc1_h_l3_missing = 'G20', lc1_h_l3_missing_level = 2 WHERE lc1 = 'G20';
