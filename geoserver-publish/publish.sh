#!/bin/bash -e

export BUILDDIR=/opt/sphinx/build

python3 -u publish_layers.py
python3 -m pytest -x -v -s -o cache_dir=/tmp/pytest_cache_dir ./tests/test_3.py -k 3b | tee -a /var/log/$LOGFILE

echo "Publishing LUCAS docs..."
bash publish_docs.sh

echo "Publishing LUCAS metadata..."
python3 -u publish_metadata.py /opt/sphinx/build/html/st_lucas_metadata.json
python3 -m pytest -x -v -s -o cache_dir=/tmp/pytest_cache_dir ./tests/test_3.py -k 3c | tee -a /var/log/$LOGFILE

echo "Creating data extracts..."
bash publish_data.sh

# Wait a little to be sure all files are written on the storage
sleep 5

# Change layers max features
PATH_TO_DATADIR=/var/local/geoserver
WORKSPACE=$POSTGRES_DB
DATASTORE=$POSTGRES_DB
MAX_FEATURES=$GS_MAX_FEATURES

for LAYER in $( cat layers.txt ); do
  echo "Changing max_features limit for layer: $LAYER"
  sed -i "s/maxFeatures>0/maxFeatures>$MAX_FEATURES/g" $PATH_TO_DATADIR/workspaces/$WORKSPACE/$DATASTORE/$LAYER/featuretype.xml
done

echo "Reloading the catalogue"
curl -u admin:$GS_ADMIN_PASSWORD -X PUT http://gs:8080/geoserver/rest/reload -H  "accept: application/json"

exit 0
