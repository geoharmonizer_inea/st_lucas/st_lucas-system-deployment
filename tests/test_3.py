import os
import pytest
import requests
import json

from utils import WS, Db

class Test3(Db):
    typename = 'sf:bugsites'

    URL=f"{os.environ['HOSTNAME']}/st_lucas_metadata.json"

    user= os.environ['MAPSERVR_USER']
    password = os.environ['MAPSERVR_PASSWORD']
    db = os.environ['POSTGRES_DB']
    host = 'db'
    port = 5432

    table_name = os.environ["TABLE_NAME"]
    st_table_name = os.environ["TABLE_NAME_ST"]

    def test_3a_001(self):
        """Perform GetCapabilities request.

        This tests case consists of checking that capabilities
        document contains expected values.
        """
        wfs = WS()
        assert wfs.identification.type == 'WFS'
        assert wfs.identification.title == 'GeoServer Web Feature Service'

    def test_3a_002(self):
        """Perform DescribeFeatureType request.
        
        This tests case consists of checking that feature type
        document contains expected values.
        """
        wfs = WS()
        data = wfs.get_schema(self.typename)

        assert data['geometry'] == 'Point'
        assert data['geometry_column'] == 'the_geom'

    def test_3a_003(self):
        """Perform GetFeature request.
        
        This tests case consists of checking that WFS return expected
        feature.
        """    
        wfs = WS()
        response = wfs.getfeature(typename=self.typename)

        assert len(response.getvalue()) > 0

    def _views(self):
        with self._connect() as conn:
            with conn.cursor() as cur:
                views = self._rows2list(
                    self._fetchall(
                        cur,
                        f"SELECT viewname FROM pg_catalog.pg_views WHERE schemaname = '{self.ms_schema}' ORDER BY viewname"
                ))

        return views

    def test_3b_001(self):
        """Perform GetCapabilities request.

        This tests case consists of checking that capabilities
        document contains all published ST_LUCAS typenames.
        """
        wfs = WS()
        layers = []
        for item in wfs.items():
            if item[0].startswith('lucas:'):
                layers.append(item[0].split(':')[1])
        layers.sort()

        assert layers == self._views()
                
    def test_3b_002(self):
        """Perform DescribeFeatureType request.
        
        This tests case consists of checking that feature type
        document contains expected value for all published LUCAS
        typenames.
        """
        wfs = WS()
        for typename in self._views():
            data = wfs.get_schema('lucas:'+typename)

            assert data['geometry'] == 'Point'
            assert data['geometry_column'] == 'geom'

    def test_3b_003(self):
        """Perform GetFeature request.
        
        This tests case consists of checking that WFS return expected
        feature for all published LUCAS typenames.
        """    
        wfs = WS()
        filter_ = '<ogc:PropertyIsEqualTo xmlns:ogc="http://www.opengis.net/ogc"><ogc:PropertyName>point_id</ogc:PropertyName><ogc:Literal>28382290</ogc:Literal></ogc:PropertyIsEqualTo>'
        for typename in self._views():
            response = wfs.getfeature(typename, filter=filter_)
            assert len(response.getvalue()) > 0
        
    def _get_md(self):
        r = requests.get(self.URL)
        return json.loads(r.content)

    def test_3c_001(self):
        """Metadata publication.

        This tests case consists of checking that JSON metadata file
        contains expected root attributes.
        """
        data = self._get_md()

        assert len(data.keys()) == 4
        assert "version" in data.keys()
        assert "max_features" in data.keys()
        assert "stats" in data.keys()
        assert "thematic_attributes" in data.keys()

        assert float(data["version"])
        assert int(data["max_features"])

    def test_3c_002(self):
        """Statistics publication.

        This tests case consists of checking that values in stats
        section corespond with values retrivied from the database.
        """
        data = self._get_md()["stats"]

        assert len(data) == 2

        with self._connect() as conn:
            with conn.cursor() as cur:
                years = self._rows2list(
                    self._fetchall(
                        cur,
                        f"SELECT DISTINCT survey_year FROM {self.ms_schema}.{self.table_name}"
                    ))
                num_points_all = self._fetchone(
                    cur,
                    f"SELECT count(*) from {self.ms_schema}.{self.table_name}"
                )
                num_points_st = self._fetchone(
                    cur,
                    f"SELECT count(*) from {self.ms_schema}.{self.st_table_name}"
                )

        # num_points
        assert "num_points" in data.keys()
        assert data["num_points"]["all"] == num_points_all
        assert data["num_points"]["st"] == num_points_st

        # years
        assert "years" in data.keys()

        assert len(data["years"].keys()) == len(years)
        for year in years:
            year = str(year)
            assert year in data["years"].keys()
            assert len(data["years"][year].keys()) == 2
            with self._connect() as conn:
                with conn.cursor() as cur:
                    countries = self._rows2list(
                        self._fetchall(
                            cur,
                            f"SELECT DISTINCT nuts0 FROM {self.ms_schema}.{self.table_name} "
                            f"WHERE survey_year = {year} and nuts0 != '-1'"
                    ))
                    num_points = self._fetchone(
                        cur,
                        f"SELECT count(*) FROM {self.ms_schema}.{self.table_name} WHERE survey_year = {year}"
                    )

                    # countries
                    assert "countries" in data["years"][year].keys()
                    for country in countries:
                        assert country in data["years"][year]["countries"].keys()
                        country_points = self._fetchone(
                            cur,
                            f"SELECT count(*) from {self.ms_schema}.{self.table_name} WHERE "
                            f"survey_year = {year} and nuts0 = '{country}'"
                        )
                        assert country_points == data["years"][year]["countries"][country]

            # num_points
            assert "num_points" in data["years"][year].keys()
            assert num_points == data["years"][year]["num_points"]

    def test_3c_003(self):
        """Thematic attributes publication.

        This tests case consists of checking that thematic attributes
        were published according to the configuration.
        """
        data = self._get_md()["thematic_attributes"]

        with open(os.path.join("conf", "thematic_attributes.json"), "r") as fd:
            mtd_data = json.load(fd)

        assert data == mtd_data["thematic_attributes"]
