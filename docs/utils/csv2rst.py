#!/usr/bin/python3

# Convert CSV file to RST notation

import argparse
import os
import csv
import glob
import sys
from pathlib import Path
from string import Template

sys.path.insert(0, '/opt/')
from db import db_version

def csv2rst(in_dir, out_dir, subdir=None):
    input_dir = Path(in_dir) / subdir if subdir else in_dir
    output_dir = Path(out_dir) / subdir if subdir else out_dir
    
    if not Path(output_dir).exists():
        os.makedirs(output_dir)

    tables = []
    for fl in glob.glob("{}/*.csv".format(input_dir)):
        title = Path(fl).stem.upper()
        if subdir is None:
            title = title.replace('_', ' ')
        tables.append(Path(fl).stem)
        if tables[-1] == 'list_of_attributes':
            widths = '15, 15, 35, 25, 10'
        else:
            widths = 'auto'
        with open(Path(output_dir) / (Path(fl).stem + ".rst"), 'w') as fd:
            fd.write(f"""{title}
{'-'*len(title)}

.. csv-table:: {title}
   :file: /opt/docs/{fl}
   :widths: {widths}
   :delim: ;
   :header-rows: 1
""")
    tables.sort()

    if subdir is None:
        return {}

    return {
        "tables_"+subdir: "\n".join(
            map(lambda x: f"   {output_dir}/{x}", tables))
    }

def publish_index(kwargs):
    with open("index.rst.template") as fd:
        index_data = Template(fd.read())

    with open("index.rst", "w") as fd:
        fd.write(
            index_data.safe_substitute(**kwargs)
        )
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('input_dir', metavar='input_dir', type=str,
                        help='Input directory')
    parser.add_argument('output_dir', metavar='output_dir', type=str,
                        help='Output directory')
    args = parser.parse_args()

    csv2rst(args.input_dir, args.output_dir)
    kwargs = {
        "db_version": db_version
    }
    kwargs.update(csv2rst(args.input_dir, args.output_dir, "coding"))
    kwargs.update(csv2rst(args.input_dir, args.output_dir, "mapping"))
    publish_index(kwargs)
