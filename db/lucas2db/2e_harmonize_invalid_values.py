#!/usr/bin/env python3

import os

from lib.change_attributes import ChangeAttributes

class InvalidValues(ChangeAttributes):
    item = "value"
    lower_values = False

    @staticmethod
    def build_sql(*args):
        args = list(args)
        if ',' in args[-1]:
            args[-1] = " IN ({})".format(
                ",".join(map(lambda x: f"'{x}'", args[-1].split(',')))
            )
        else:
            args[-1] = f"= '{args[-1]}'"
        print("UPDATE lucas{0} SET {1}=-1 WHERE {1} {2};".format(
                *args)
        )

if __name__ == "__main__":
    cha = InvalidValues()
    cha.run()
